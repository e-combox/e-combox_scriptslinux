# Version 4 de l'e-comBox sur Linux

**La version v4** :

- la version courante est la version 4.3
- permet d'utiliser l'e-comBox derrière un Reverse Proxy externe (voir exemple de configuration ci-dessous), ce qui permet de ne dédier qu'une seule adresse IP publique pour l'ensemble des instances ;
- permet de s'affranchir des ports même s'il n'y a pas de Reverse Proxy externe (seul le port d'accès à l'application est exposé, ce dernier peut être le port par défaut (443) du HTTPS ;
- intègre HTTPS ;
- dispose de deux méthodes d'authentification, la première basée sur des comptes créés via Portainer, la deuxième basée sur une authentification OpenID Connect (version 4.2) ;
- améliore l'authentification (un professeur identifié sur l'interface ne gérera que ses propres sites) ;
- ne nécessite plus obligatoirement un client tel que Filezilla pour l'accès aux sources via le SFTP ;
- intègre les mentions légales ;
- oblige à changer le mot de passe "admin" de Portainer ;
- permet de réinitialiser le mot de passe "admin" de Portainer ;
- intègre une version d'installation entièrement automatisée (l'installation via un outil de déploiement comme Ansible est possible) ;
- permet de ne restaurer qu'un seul site ;
- automatise la sauvegarde/restauration ;
- permet de partager ses propres modèles de sites : le partage de modèles de sites est possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub ;
- permet de récupérer un modèle de site créé par un utilisateur de la communauté : il est désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox ;
- donne la possibilité de réaliser une action (supprimer, stopper, démarrer) sur plusieurs sites en les sélectionnant ;
- permet de réinitialiser un site.

**Pour une documentation plus complète** : <https://wiki-ecombox.btssio.corsica/>

**Il s'agit ici des scripts qui permettent d'installer l'application e-comBox sur un serveur Linux.
Ces scripts ont été testés sur les versions Linux (obligatoirement 64 bits) suivantes :**

- Ubuntu (à partir la version 18.04) ;
- Debian 9 ;
- Debian 10 ;
- Debian 11
- Debian 12 (actuellement stable).

<!-- TOC -->

- [Version 4 de l'e-comBox sur Linux](#version-4-de-le-combox-sur-linux)
  - [1. Configuration du reverse-proxy externe - Exemples](#1-configuration-du-reverse-proxy-externe---exemples)
    - [1.1. Avec Nginx](#11-avec-nginx)
    - [1.2. Avec Apache](#12-avec-apache)
  - [2. Installation automatisée de l'e-comBox](#2-installation-automatisée-de-le-combox)
    - [2.1. Fonctionnement](#21-fonctionnement)
    - [2.2. Le nouveau fichier param.conf](#22-le-nouveau-fichier-paramconf)
    - [2.3. Récupération (ou modification) de configure\_application.sh](#23-récupération-ou-modification-de-configure_applicationsh)
    - [2.4. Installation ou re-configuration de l'application](#24-installation-ou-re-configuration-de-lapplication)
  - [3. Gestion de l'authentification](#3-gestion-de-lauthentification)

<!-- /TOC -->

## 1. Configuration du reverse-proxy externe - Exemples

> Note
>
> La configuration d'un reverse-proxy externe n'est utile que si vous avez plusieurs instances de l'application et que vous voulez les rendre accessible via une seule adresse IP ou un même nom de domaine.

**Adresse IP privée du serveur** sur lequel est installée e-comBox : 172.31.40.109

**Nom de domaine** : ecb.nom.domaine (cela fonctionne également avec une adresse IP publique)

**Chemin d'accès** : rne

On suppose que l'on conserve les ports par défaut à savoir (ces derniers sont totalement transparents pour l'utilisateur si un reverse-proxy externe est utilisé sinon il utilisera le "PORT_RP") :

- Port utilisé pour l'accès à Portainer, à l'interface et aux sites (reverse-proxy Nginx interne à l'application) :
  - PORT_RP=8800
- Port utilisé pour un accès direct à Portainer (utile uniquement si le reverse-proxy interne ne fonctionne pas) :
  - PORT_PORTAINER=8880
- Port utilisé pour le registry :
  - PORT_REGISTRY=5443

**L'interface de l'e-comBox est accessible** via l'URL : <https://ecb.nom.domaine/rne/app/>

**L'interface de Portainer est accessible** via l'URL : <https://ecb.nom.domaine/rne/portainer/>

**Les sites sont accessibles** via une URL sous la forme : <https://ecb.nom.domaine/rne/nom_du_site/>

### 1.1. Avec Nginx

Ci-dessous **le fichier default.conf** de Nginx :

```server {
  server_name ecb.nom.domaine;
  listen 80; # Éventuellement
  listen 443 ssl;

  # Chemin vers les certificats (le nom des fichiers certificats est libre)
  ssl_certificate ssl/ecb.nom.domaine.crt.pem;
  ssl_certificate_key ssl/ecb.nom.domaine.key.pem;

  ...

  location / {
    ...
    error_page 404 = @404;
    return 404;
    error_page 497 = @497;
    return 497;

  }

  ….
  # Pour Odoo sinon les CSS ne sont pas chargés
  location @404 {
     add_header Vary Referer;
     # À répéter pour chaque instance
     if ($http_referer ~ ://[^/]*(/rne/).*) {
     proxy_pass https://172.31.40.109:8800;
     }
  }
  
  # Pour Odoo avec le HTTPS sinon les CSS ne sont pas chargés
  location @497 {
     add_header Vary Referer;
     # À répéter pour chaque instance
     if ($http_referer ~ ://[^/]*(/rne/).*) {
     proxy_pass https://172.31.40.109:8800;
     }
  }
 
  location /rne/ {
     proxy_pass https://172.31.40.109:8800;
     proxy_redirect off;
  }

}
```

### 1.2. Avec Apache

Installer le module "proxy_wstunnel" : a2enmod proxy_wstunnel

Ci-dessous **le fichier de configuration** d'Apache :

``` text
<VirtualHost *:443>
  ServerName ecb.nom.domaine

  ...
  
  # Gestion du mode SSL (le nom des fichiers certificats est libre)
  SSLEngine on
  SSLCertificateFile "ssl/ecb.nom.domaine.crt.pem"
  SSLCertificateKeyFile "ssl/ecb.nom.domaine.key.pem"
  ...
  
  ProxyPreserveHost On
  RewriteEngine On

  # Pour l'accès aux consoles des conteneurs via Portainer (ne pas oublier d’ajouter le module proxy_wstunnel)
  ProxyPassMatch "/rne/portainer/api/websocket/(.*)" wss://172.31.40.109:8880/api/websocket/$1

  # Pour l'accès à l'interface de l'instance
  ProxyPass /rne/ https://172.31.40.109:8800/rne/
  ProxyPassReverse /rne/ https://172.31.40.109:8800/rne/

  # Pour le chargement CSS des Odoo
  RewriteCond "%{HTTP_REFERER}" "://[^/]*(/rne/).*" [NC]
  RewriteRule ^(.*)$ https://172.31.40.109$1 [P]
  ...

  </VirtualHost>
```

> Note
>
> Si aucune reverse-proxy externe n'est configuré :
>
> - **L'interface de l'e-comBox serait accessible** via l'URL : <https://ecb.nom.domaine:8800/app/>
>
> - **L'interface de Portainer serait accessible** via l'URL : <https://ecb.nom.domaine:8800/portainer/>
>
> - **Les sites seraient accessibles** via une URL sous la forme : <https://ecb.nom.domaine:8800/nom_du_site/>
>
> Si le nom de domaine n'est pas renseigné :
>
> - **L'interface de l'e-comBox serait accessible** via l'URL : <https://172.31.40.109:8800/app/>
>
> - **L'interface de Portainer serait accessible** via l'URL : <https://172.31.40.109:8800/portainer/>
>
> - **Les sites seraient accessibles** via une URL sous la forme : <https://172.31.40.109:8800/nom_du_site/>

## 2. Installation automatisée de l'e-comBox

> Note
>
> L'installation classique n'est plus possible à partir de la version 4.1.
> **ATTENTION** : à partir de la version des scripts dev.1 et 4.1.1 (20/04/2023), tous les "B" majuscule dans les noms de dossier basculent en "b" minuscule :
>
> - le dossier d'installation /opt/e-comBox devient /opt/e-combox
> - le dossier /opt/ecomBox/e-comBox_portainer devient /opt/ecombox/e-combox_portainer
> - le dossier /opt/ecomBox/e-comBox_reverseproxy devient /opt/ecombox/e-combox_reverseproxy

### 2.1. Fonctionnement

Il s'agit d'une installation automatique (nouvelle fonctionnalité intégrée dans la version 4.1). Le script installe l'e-comBox à partir du fichier param.conf (qui a été enrichi de nouveaux paramètres) sans interaction avec l'utilisateur.

Le fichier /opt/e-combox/param.conf sera automatiquement rempli avec les éléments passés en ligne de commande ou avec un fichier /opt/e-combox/param.conf existant. Si ce dernier n'existe pas déjà il sera créé.

> Note
>
> Les scripts install_linux_e-combox.sh et configure_application.sh peuvent être exécutés en mettant en paramètre le chemin vers un fichier contenant les paramètres utiles ou passer directement les paramètres sur le ligne de commande (voir exemples plus loin).
>
> Cela peut être utile pour une première installation et pour configurer chaque instance avec un outil de déploiement tel qu'Ansible.
>
> En ce qui concerne les certificats, si l’on passe par un reverse proxy, les certificats de ce dernier sont propagés et il n’est pas besoin de les installer sur les instances (vous pouvez donc laisser le certificat se créer automatiquement comme proposé dans le script).

### 2.2. Le nouveau fichier param.conf

Ci-dessous le **fichier /opt/e-combox/param.conf** correctement rempli au regard de l'exemple fourni (à noter que la plupart des paramètres sont renseignés par défaut):

```bash
#!/bin/bash

# Définition des paramètres utiles à l'application.

# Version du fichier paramètre (ne pas changer la valeur).
VERSION_PARAM="dev.0"

# Validation (ou non) de la licence.
# Si cette variable n'a pas la valeur "true", l'application ne pourra pas être installée.
# La licence est consultable ici : https://forge.aeif.fr/e-combox/e-combox_scriptslinux/-/raw/v4/LICENCE.
# Les conditions plus générales de la licence CeCILL sont définies sur le site : http://www.cecill.info.
VALIDATION_LICENCE="true"

# Mot de passe pour la connexion en admin à e-comBox et Portainer.
# Le mot de passe doit contenir au moins de 12 caractères sans espace.
# Le mot de passe ne doit pas contenir les caractères spéciaux suivants : \ " ` $
# Il ne peut pas être égal à "portnairAdmin" qui est connu de tous
# Le système met le mot de passe à jour avec celui saisi si les deux conditions suivantes sont réunies
# 1 - À l'installation ou à la réinitialisation de l'application
# 2 - Si le mot de passe renseigné a été au préalable modifié sur Portainer
# Ce mot de passe en clair sera chiffré, puis supprimé à l'installation, à la réinitialisation ou à la reconfiguration de l'application
# C'est donc normal qu'une fois l'application installée, réinitialisée ou mise à jour, ce paramètre apparaisse vide.
MDP_PORTAINER=""

# Adresse IP privée ou nom de domaine correspondant à une adresse IP privée.
# Si vous saisissez un nom de domaine, celui-ci doit pouvoir être résolu.
# Si aucun domaine n'est configuré, les sites seront accessibles à partir du réseau local via cette adresse IP privée.
# L'adresse IP privée doit obligatoirement être configurée même si le paramètre suivant est renseigné.
ADRESSE_IP_PRIVEE=""

# Adresse IP publique ou nom de domaine correspondant à une adresse IP publique.
# Si vous saisissez un nom de domaine, celui-ci doit pouvoir être résolu.
# Il s'agit du nom de domaine ou de l'adresse IP qui sera utilisée pour la connexion à l'interface, à Portainer et aux sites à la place de l'adresse IP privée.
# Aucune valeur ne doit donc être saisie ici si vous ne voulez pas cela, notamment si le serveur ne doit pas être accessible de l'extérieur.
DOMAINE=""

# Utilisation d'un Reverse-Proxy externe configuré par vos soins - O/N (N par défaut).
# Si vous passez par un reverse proxy externe, mettre "O".
# Les minuscules sont également acceptées.
RP_EXT="N"

# Chemin d'accès éventuel (en cas d'utilisation d'un Reverse-Proxy externe).
# Ne pas mettre de "/" dans le chemin.
CHEMIN=""

# Dossier en chemin absolu contenant la clé secrète qui chiffre le mot de passe de Portainer.
# Par défaut /opt/e-combox, il est conseillé de le modifier.
# En cas de modification après installation, ne pas oublier de déplacer le fichier contenant la clé secrète.
DOSSIER_MDP_KEY="/opt/e-combox"

# Dossier en chemin absolu contenant le mot de passe chiffré.
# Par défaut /opt/e-combox, il est conseillé de le modifier.
# En cas de modification après installation, ne pas oublier de déplacer le fichier contenant le mot de passe chiffré.
DOSSIER_MDP_CHIFFRE="/opt/e-combox"

# Port utilisé pour un accès direct à Portainer (8880 par défaut).
# Ce port n'a vocation à n'être utilisé qu'en cas de dépannage si l'accès via le reverse-proxy est impossible.
PORT_PORTAINER="8880"

# Port utilisé pour l'accès à Portainer, à l'interface et aux sites (8800 par défaut).
# Il s'agit du seul port exposé.
# Si aucun service Web n'écoute sur le port 443 de votre serveur, ce dernier peut être utilisé.
PORT_RP="8800"

# Port utilisé pour le registry (5443 par défaut).
PORT_REGISTRY="5443"

# Adresse du Proxy.
# Saisissez ip-proxy:port.
ADRESSE_PROXY=""

# Hôtes à ignorer par le proxy.
# Ce paramètre n'est à renseigner que si une adresse de Proxy a été saisie.
# Saisissez le ou les hôtes séparés par une virgule sans espace.
NO_PROXY=""

# Adresse du réseau interne à Docker.
# Cette adresse réseau doit être éventuellement modifiée notamment pour adapter le masque.
# Un site nécessite 2 adresses IP réseau. 
# Compte tenu des conteneurs utilitaires, avec un masque en /24, le nombre de sites est limité à 120.
# Cette adresse peut être modifiée ultérieurement.
NET_ECB="192.168.97.0/24"

# Suppression des images non associées à un site en cours d'exécution (false par défaut) 
# Cela permet de gagner de l'espace mais aussi de supprimer des images qui ne seront plus jamais utilisées
# Le premier démarrage des sites sera un peu plus long car les images correspondantes devront être de nouveau téléchargées.
# Définir le paramètre à "true" si vous voulez supprimer ces images (conseillé lors d'une migration).
DEL_IMAGES="false"

# Les 2 variables qui suivent sont utiles pour donner le chemin vers les éléments pour mettre en place un certificat existant.
# Vous pouvez les laisser vides dans trois cas.
# Cas 1 : si vous passez par un reverse proxy externe, les certificats configurés dans ce dernier sont propagés.
# Cas 2 : l'accès à l'e-comBox ne va se faire que via le réseau local.
# Cas 3  vous ne disposez pas de certificat pour votre nom de domaine.
# À noter que le script manage_certificat.sh crée un certificat Let's Encrypt et permet de mettre à jour l'application
# si une des 6 variables qui suivent ont été modifiées
# Un certificat auto-signé se créera alors automatiquement via les 4 variables qui suivent.

# Fichier certificat existant avec le chemin complet /chemin/fichier.crt ou /chemin/fichier.pem.
CHEMIN_CERT=""

# Fichier de la clé privée existante correspondante au certificat avec le chemin complet /chemin/fichier.key.
CHEMIN_KEY=""

# Uniquement si utilisation de manage_certificat.sh pour créer un certificat Let's Encrypt
# Adresse de courriel non obligatoire pour créer le certificat (mais recommandé par Lets'encrypt)
MAIL=""

# Les 4 variables suivantes sont utiles pour créer un certificat auto signé.
# Elles sont utilisés pour le certificat du registry local.
# Elles sont également utilisées pour le reverse proxy (Nginx) si CHEMIN_CERT et CHEMIN_KEY sont vides.
# Même si vous avez un certificat existant, elles doivent obligatoirement être renseignées.
# Vous pouvez les modifier comme il vous convient.

# Code Pays sur 2 lettres.
CODE_PAYS="FR"

# Nom Pays.
NOM_PAYS="France"

# Nom région.
NOM_REGION="Corse"

# Nom organisation.
NOM_ORGANISATION="ReseauCerta"

# Ajout du mode d'authentification pour les comptes non-admin basés sur le protocole OAuth (false par défaut).
# Si false, il n'est pas tenu compte des paramètres qui suivent.
# Si true, vous devez fournir la valeur des paramètres qui suivent.
# Veuillez-vous référer à votre fournisseur OAuth pour déterminer les bonnes valeurs.
OAUTH_ENABLE="false"

# true ou false
# Lors de l'utilisation de SSO (variable à "true"), le fournisseur OAuth n'est pas obligé de demander des informations d'identification.
SSO_ENABLE=""

# Identifiant public de l'application OAuth.
CLIENT_ID=""

# Client Secret
CLIENT_SECRET=""

# URL utilisée pour s'authentifier auprès du fournisseur OAuth.
# L'utilisateur sera redirigé vers la page d'authentification du fournisseur d'identité.
AUTHORIZATION_URL=""

# URL utilisée par Portainer pour échanger un code d'authentification OAuth valide contre un token d'accès.
ACCESS_TOKEN_URL=""

# URL utilisée par Portainer pour récupérer des informations sur l'utilisateur authentifié.
RESOURCE_URL=""

# URL utilisée par Portainer pour rediriger l'utilisateur vers le fournisseur OAuth afin de déconnecter l'utilisateur de la session du fournisseur d'identité.
LOGOUT_URL=""

# Identifiant qui sera utilisé par Portainer pour créer un compte pour l'utilisateur authentifié. 
# Extrait du serveur de ressources spécifié via le champ URL de la ressource.
USER_IDENTIFIER=""

# Étendues requises par le fournisseur OAuth pour récupérer des informations sur l'utilisateur authentifié.
SCOPES=""

# Fichier certificat du fournisseur 0Auth avec le chemin complet /chemin/fichier.pem.
# Ce dernier doit potentiellement être intégré à Portainer
CERT_OAUTH=""

# Les variables qui suivent servent à alimenter automatiquement les mentions légales
# Si ces dernières n'existent pas ou si le script modif_mentions_legales.sh est exécuté
# Les mentions légales peuvent également être modifiées via l'interface de l'e-comBox
PROPRIETAIRE=""
ADRESSE_POSTALE=""
IMMATRICULATION=""
HEBERGEUR=""
RESPONSABLE_TRAITEMENT=""
ADMIN_SERVICE=""
DPO=""

```

### 2.3. Récupération (ou modification) de configure_application.sh

À partir de la version 4.1, il suffit de télécharger et d'exécuter (avec d'éventuelles options) le script configure_application.sh.

Après avoir pris les précautions d'usage si le serveur passe par un proxy (voir [ici](https://wiki-ecombox.btssio.corsica/index.php/Installation_sur_Linux_-_v4#Pr%C3%A9alables)) :

- curl -fsSL <https://forge.apps.education.fr/e-combox/e-combox_scriptslinux/raw/4.3/configure_application.sh> -o configure_application.sh

> **Note** : si vous avez déjà une version 4.1 ou 4.2 installée, vous pouvez également remplacer la valeur de la variable BRANCHE 4.1/4.2 par 4.3 dans /opt/e-combox/configure_application.sh.

### 2.4. Installation ou re-configuration de l'application

Lancer le script avec les paramètres nécessaires (pour une première configuration le mot de passe et au moins l'adresse IP privée doivent être renseignés).

> **Note** S'il s'agit d'une simple migration à partir de la version 4.1 et que les paramètres dans /opt/e-combox/param.conf doivent rester identiques, il suffit de lancer le script sans options supplémentaires (bash /opt/e-combox/configure_application.sh)

Pour remplir le param.conf sans être obligé de l'éditer, eu égard à l'exemple :

- bash /opt/e-combox/configure_application.sh -i "172.31.40.109" -d "ecb.nom.domaine" -c "rne" -r "O" -p "mdp_portainer"

Les valeurs des paramètres peuvent également être renseignés directement dans /opt/e-combox/param.conf ou être passés (avec l'option -f) via un fichier. Exemple de fichier :

```bash
ADRESSE_IP_PRIVEE="172.31.40.109"
DOMAINE="ecb.nom.domaine"
RP_EXT="O"
CHEMIN="rne"
MDP_PORTAINER="mdp_portainer"
DOSSIER_MDP_KEY="/etc/ssl/certs/ecombox"
DOSSIER_MDP_CHIFFRE="/etc/ssl/certs/ecombox"
...
```

> **Note** : tous les paramètres peuvent être modifiés via ce fichier. Dans l'exemple ci-dessus, les paramètres DOSSIER_MDP_KEY et DOSSIER_MDP_CHIFFRE seront également modifiés.

Il peut être ensuite appelé via la commande : bash /opt/e-combox/configure_application.sh -f "param.conf"

```bash
Usage: configure_application.sh [-f "valeur"] [-i "valeur"] [-d "valeur"] [-r "valeur"] [-c "valeur"] [-p "valeur"] [s | b] [-h]
    -f    Chemin vers le fichier de paramètre  [-f "/chemin/nom_fichier"]
    -i    Adresse IP privée ou nom de domaine correspondant à une adresse IP privée  [-i "@IP_PRIVEE" | -i "nom_domaine"]
    -d    Adresse IP publique ou nom de domaine correspondant à une adresse IP publique  [-d @IP_PUBLIQUE | -d nom_domaine]. Pour supprimer une adresse IP publique ou un domaine existant -d ""
    -r    Passage par un Reverse-Proxy externe ou non  [-r "O" | -r "N"]
    -c    Chemin en cas de Reverse-Proxy externe [-c "chemin"]. Pour supprimer un chemin existant -c ""
    -p    Mot de passe de Portainer [-p "mot_de_passe"]. 
          Les caractères suivants " $ ` \ & | ! [space] ne peuvent pas être utilisés en ligne de commande.
    -s    Met la variable de suppression des images à "true" le temps d'exécution du script [-s]
    -b    Met la variable de suppression des images à "false" le temps d'exécution du script [-n]  
    -h    Détail des options
```

## 3. Gestion de l'authentification

**L'authentification peut se réaliser via Portainer**. L'administrateur doit se connecter sur Portainer et créer des utilisateurs dans le groupe "Prof" automatiquement créé à l'installation.

Le professeur peut ensuite modifier son mot de passe via l'application.

Les professeurs ne voient et ne peuvent agir que sur les sites qu'ils ont eux-même créés.

Voir ici la documentation complète : <https://wiki-ecombox.btssio.corsica/index.php/Gestion_Utilisateurs>

> **Note** : le compte "admin" de Portainer peut se connecter sur l'e-comBox et créer des sites mais cela n'est pas conseillé.

**L'authentification peut être configuré pour utiliser un serveur d'authentification basé sur le protocole OAuth** :

- <https://wiki-ecombox.btssio.corsica/index.php/Gestion_Utilisateurs>
- <https://wiki-ecombox.btssio.corsica/index.php/Usage_des_scripts_-_v4#Configuration_de_l>'authentification_openID_Connect_:_configure_oauth.sh_(version_4.2)
