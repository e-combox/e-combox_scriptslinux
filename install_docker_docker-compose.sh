#!/bin/bash
# Ce script lance des scripts qui automatise l'installation de Docker et Docker Compose
# shellcheck disable=SC2034

# Couleurs
COLTITRE="\033[1;35m"  # Rose
COLPARTIE="\033[1;34m" # Bleu
COLTXT="\033[0;37m"    # Gris
COLCHOIX="\033[1;33m"  # Jaune
COLDEFAUT="\033[0;33m" # Brun-jaune
COLSAISIE="\033[1;32m" # Vert
COLCMD="\033[1;37m"    # Blanc
COLERREUR="\033[1;31m" # Rouge
COLINFO="\033[0;36m"   # Cyan
COLSTOP="\033[1;31m"   # Rouge

# Installation de Docker et docker compose

# Pour l'installation de Docker
# Utilisation du script officiel fourni par Docker
# https://github.com/docker/docker-install pour Docker

echo -e "Installation de Docker et Docker Compose (cela peut prendre un peu de temps)... En cours"

apt-get update >/dev/null && apt-get install -y curl git >/dev/null
apt-get install -y apt-transport-https ca-certificates gnupg2 software-properties-common >/dev/null
curl -fsSL https://get.docker.com -o install-docker.sh >/dev/null
sh install-docker.sh
rm install-docker.sh

# Attente pour que Docker soit complètement opérationnel et relancer le deamon docker
#sleep 5
systemctl daemon-reload
#systemctl restart docker

if (docker -v >/dev/null); then
    echo -e "Installation de Docker... Succès"
else
    echo -e "$COLSTOP"
    echo -e "Installation... Échec${COLCMD}"
    exit
fi

# Docker compose v2 est maintenant intégré dans la version de Docker 

if (docker compose version >/dev/null); then
    echo -e "Installation de Docker Compose... Succès"
else
    echo -e "$COLSTOP"
    echo -e "Installation de Docker Compose... Échec${COLCMD}"
    exit
fi