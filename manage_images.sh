#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, de fonctions et de param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$DOSSIER_INSTALL"/param.conf

echo -e "\nGestion des images le $(date)\n" >>"$LOGFILE"

ACTIVE_OPTION() {
    TYPE_SITE=$1

    #Suppression des images
    if [ "$delete" = "true" ]; then
        for image in $(docker images --format "{{.Repository}}:{{.Tag}}" | grep "$TYPE_SITE"); do
            docker rmi "$image"
        done 2>>"$LOGFILE"
        if [ "$TYPE_SITE" = "odoo" ]; then
            for image in $(docker images --format "{{.Repository}}:{{.Tag}}" | grep "postgres-"); do
                docker rmi "$image"
            done 2>>"$LOGFILE"
        fi
        if [ "$(docker images -qf dangling=true)" ]; then
            # shellcheck disable=SC2046
            docker rmi $(docker images -qf dangling=true) >>"$LOGFILE" 2>&1
            echo -e "Images orphelines... Supprimées"
        fi

    # Mise à jour des images
    elif [ "$update" = "true" ]; then
        for image in $(docker images --format "{{.Repository}}:{{.Tag}}" | grep "$TYPE_SITE"); do
            docker pull "$image"
        done 2>>"$LOGFILE"
        if [ "$TYPE_SITE" = "odoo" ]; then
            for image in $(docker images --format "{{.Repository}}:{{.Tag}}" | grep "postgres-"); do
                docker pull "$image"
            done 2>>"$LOGFILE"
        fi
    fi
}

PULL_IMAGES() {

    echo -e "$COLINFO"
    echo -e "L'opération de téléchargement de toutes les images peut prendre du temps..."
    echo -e "$COLCMD"

    # Répertoire contenant les fichiers docker-compose
    DOCKER_COMPOSE_DIR="$DOSSIER_INSTALL/temp"
    mkdir $DOCKER_COMPOSE_DIR

    # Récupération des docker-compose
    cd "$DOCKER_COMPOSE_DIR" || exit
    git clone -b "$BRANCHE" https://"$FORGE"/e-combox/e-comBox_docker-compose.git >>"$LOGFILE" 2>&1

    # Parcours des fichiers docker-compose
    for file in "$DOCKER_COMPOSE_DIR"/e-comBox_docker-compose/*.yml; do
        # Conversion du fichier YAML en JSON avec python3-yaml et extraction des noms des images avec jq
        images=$(python3 -c "import yaml, sys, json; json.dump(yaml.load(open(sys.argv[1]), Loader=yaml.FullLoader), sys.stdout)" "${file}" | jq -r '.services | to_entries[] | .value | .image')
        images_uniques=$(echo "$images" | sort | uniq)

        # Parcours des images extraites
        while read -r image; do
            # Vérification si l'image commence par reseaucerta
            if [[ "${image}" == reseaucerta* ]] || [[ "${image}" == postgres* ]]; then
                # Téléchargement de l'image si elle n'est pas déjà présente
                if ! (docker images --format "{{.Repository}}:{{.Tag}}" | grep "$image" >/dev/null); then
                    docker pull "$image" >>"$LOGFILE" 2>&1
                    echo -e "Téléchargement de l'image $image... Fait"
                else
                    echo -e "Image $image... Existe déjà"
                fi
            fi
        done <<<"${images_uniques}"
    done
    rm -rf "$DOCKER_COMPOSE_DIR"

}

SHOW_USAGE() {
    echo -e "\nCe script permet de télécharger, supprimer et de gérer les images de l'e-comBox."
    echo -e "Des options doivent être renseignées en ligne de commande (voir Usage ci-après).\n"
    echo -e "Usage: bash $0 -d|u|t [-a] [-p] [-w] [-b] [-m] [-s] [-o] [-k] [-b] [-i \"nom_image\"] [-f \"nom_image\" | -f \"type_image\"] [h]"
    echo -e "\t-d\t\tSuppression des images non associées à un site en cours d'exécution [-d]"
    echo -e "\t-u\t\tMise à jour des images. Les sites doivent être redémarrés pour que la mise à jour soit effective [-u]"
    echo -e "\t-a\t\tL'action porte sur toutes les images existantes."
    echo -e "\t-p\t\tSuppression ou mise à jour des images Prestashop [-p]"
    echo -e "\t-w\t\tSuppression ou mise à jour des images WooCommerce [-w]"
    echo -e "\t-b\t\tSuppression ou mise à jour des images Blog [-b]"
    echo -e "\t-m\t\tSuppression ou mise à jour des images Mautic [-m]"
    echo -e "\t-s\t\tSuppression ou mise à jour des images Suite CRM [-s]"
    echo -e "\t-o\t\tSuppression ou mise à jour des images Odoo [-o]"
    echo -e "\t-k\t\tSuppression ou mise à jour des images Kanboard [-k]"
    echo -e "\t-r\t\tSuppression ou mise à jour des images HumHub [-r]"
    echo -e "\t-i\t\tSuppression ou mise à jour d'une image spécifique [-i \"nom_image\"]"
    echo -e "\t-f\t\tMise à jour d'une image utilitaire [-f \"nom_image\" | -f \"type_image\"]"
    echo -e "\t-t\t\tTéléchargement de toutes les images des sites (utile pour accélérer la création du premier site correspondant aux images) [-t]"
    echo -e "\t-h\t\tDétail des options"
}

if [ $# -eq 0 ]; then
    echo -e "${COLSTOP}Une des deux options \"- d\" ou \"- u\" doit obligatoirement être renseignée.\n${COLCMD}"
    echo -e "\nAucun option passée en ligne de commande.\n" >>"$LOGFILE"
    SHOW_USAGE
    exit
fi

# Initialisation des paramètres passés au script
while getopts "duapwbmsokri:f:th" option; do
    # d: suppression des images
    # u: mise à jour des images
    # a: l'action porte sur toutes les images existantes qui ne sont pas associées à un site en cours d'exécution
    # p: suppression ou mise à jour des images Prestashop [-p]
    # w: suppression ou mise à jour des images WooCommerce [-w]
    # b: suppression ou mise à jour des images Blog [-b]
    # m: suppression ou mise à jour des images Mautic [-m]
    # s: suppression ou mise à jour des images Suite CRM [-s]
    # o: suppression ou mise à jour des images Odoo [-o]
    # k: suppression ou mise à jour des images Kanboard [-k]
    # r: suppression ou mise à jour des images HumHub [-r]
    # i: suppression ou mise à jour d'une image spécifique [-f "nom_image"]"
    # f: suppression ou mise à jour d'une image ou d'un type d'image spécifique [-f "nom_image" || -f "nom_type_image"]
    # t: téléchargement de toutes les images des sites (utile pour accélérer la création du premier site correspondant aux images)
    # h: détail des options

    case $option in
    d) delete="true" ;;
    u) update="true" ;;
    a) full="true" ;;
    p) ACTIVE_OPTION prestashop ;;
    w) ACTIVE_OPTION woocommerce ;;
    b) ACTIVE_OPTION blog ;;
    m) ACTIVE_OPTION mautic ;;
    s) ACTIVE_OPTION suitecrm ;;
    o) ACTIVE_OPTION odoo ;;
    k) ACTIVE_OPTION kanboard ;;
    r) ACTIVE_OPTION humhub ;;
    i) ACTIVE_OPTION "$OPTARG" ;;
    f)
        if [[ "$OPTARG" =~ ecombox || "$OPTARG" =~ e-combox || "$OPTARG" =~ docker-gen || "$OPTARG" =~ registry || "$OPTARG" =~ nginx || "$OPTARG" =~ portainer || "$OPTARG" =~ git ]]; then
            image_utilitaire="$OPTARG"
        else
            echo -e "${COLSTOP}\nL'image n'existe pas.${COLCMD}"
            echo -e "\nL'image n'existe pas." >>"$LOGFILE"
        fi
        ;;
    t)
        pull="true"
        PULL_IMAGES
        ;;
    h)
        SHOW_USAGE
        exit
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        SHOW_USAGE
        exit 1
        ;;
    esac
done

# Pour empêcher l'utilisation simultanée des options "-d", "-u" et "-t"
if [ -n "$delete" ] && [ -n "$update" ] && [ -n "$pull" ]; then
    echo -e "${COLSTOP}\nLes options \"-d\", \"-u\" et \"-t\" ne peuvent pas s'utiliser simultanément\n${COLCMD}."
    SHOW_USAGE
    exit
fi

# Pour forcer l'utilisation d'une des trois options "-d" ou "-u"
if [ -z "$delete" ] && [ -z "$update" ] && [ -z "$pull" ]; then
    echo -e "${COLSTOP}\nUne des trois options \"-d\" ou \"-u\" ou \"-t\" doit obligatoirement être utilisée\n${COLCMD}."
    SHOW_USAGE
    exit
fi

# Mise à jour d'une image utilitaire
if [ -n "$image_utilitaire" ]; then
    case "$image_utilitaire" in
    *ecombox* | *e-combox*)
        if [ -n "$update" ]; then
            echo -e "\nMise à jour de l'e-comBox le $(date)\n" >>"$LOGFILE"
            CONF_ECB
        else
            echo -e "${COLSTOP}\nIl n'est pas possible de supprimer cette image${COLCMD}"
            exit
        fi
        ;;
    *git*)
        if [ -n "$update" ]; then
            echo -e "Téléchargement de reseaucerta/git-http-server... En cours"
            docker pull reseaucerta/git-http-server:"$TAG" >>"$LOGFILE"
            echo -e "Téléchargement de reseaucerta/git-http-server... Fait"
            echo -e "Redémarrage du serveur git... En cours"
            {
                docker stop e-combox_gitserver
                docker rm e-combox_gitserver
                docker volume rm e-combox_git-data
                docker run -d -p 443 --restart=always --name e-combox_gitserver -v e-combox_git-data:/git -e VIRTUAL_HOST=git -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 --network=bridge_e-combox reseaucerta/git-http-server:"$TAG"

            } >>"$LOGFILE"
            echo -e "Redémarrage du serveur git... Fait"
        else
            echo -e "${COLSTOP}\nIl n'est pas possible de supprimer cette image${COLCMD}"
            exit
        fi
        ;;
    *docker-gen* | *nginx* | reverseproxy | rp)
        if [ -n "$update" ]; then
            echo -e "Téléchargement de docker-gen... En cours"
            docker pull reseaucerta/docker-gen:"$TAG" >>"$LOGFILE"
            echo -e "Téléchargement de nginx... En cours"
            docker pull reseaucerta/nginx:"$TAG" >>"$LOGFILE"
            echo -e "Téléchargement de nginx... Fait"
            cd "$DOSSIER_RP" || exit
            docker compose down
            docker volume rm e-combox_reverseproxy_nginx-html
            docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates
            curl -fsSL "$DEPOT_GITLAB_RP_EXT"/raw/"$BRANCHE"/docker-compose.yml -o docker-compose.yml
            docker compose up -d
        else
            echo -e "${COLSTOP}\nIl n'est pas possible de supprimer cette image${COLCMD}"
            exit
        fi
        ;;
    *registry*)
        if [ -n "$update" ]; then
            echo -e "Téléchargement du registry... En cours"
            docker pull reseaucerta/registry:"$TAG" >>"$LOGFILE"
            echo -e "Téléchargement du registry... Fait"
            echo -e "Arrêt du registry... En cours"
            docker stop e-combox_registry >>"$LOGFILE"
            docker rm e-combox_registry >>"$LOGFILE"
            echo -e "Lancement du registry... En cours"
            docker run -d -p "$PORT_REGISTRY":443 --restart=always --name e-combox_registry -v "$DOSSIER_CERTS"/registry:/certs -v registry_data:/var/lib/registry -e VIRTUAL_HOST=registry -e REGISTRY_STORAGE_DELETE_ENABLED=true -e REGISTRY_HTTP_ADDR=0.0.0.0:443 -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/localhost.crt -e REGISTRY_HTTP_TLS_KEY=/certs/localhost.key -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 reseaucerta/registry:"$TAG" >>"$LOGFILE" 2>&1
            if [ $? -eq 0 ]; then
                echo -e "Démarrage du registry... Succès"
                echo -e "Le registry a été lancé.\n" >>"$LOGFILE"
            else
                echo -e "$CLOSTOP"
                echo -e "Démarrage du registry... Échec"
                echo -e "$COLCMD"
                exit
            fi
            # Déconnexion du registry au réseau de l'e-combox
            docker network disconnect bridge e-combox_registry >>"$LOGFILE" 2>&1
            # Connexion du registry au réseau de l'e-combox
            docker network connect bridge_e-combox e-combox_registry >>"$LOGFILE" 2>&1
        else
            echo -e "${COLSTOP}\nIl n'est pas possible de supprimer cette image${COLCMD}"
            exit
        fi
        ;;
    portainer/portainer-ce:*)
        if [ -n "$update" ]; then
            # Vérifie si l'image existe
            echo -e "Mise à jour de Portainer... En cours"
            if (docker pull "$image_utilitaire" >>"$LOGFILE" 2<&1); then
                docker stop portainer-app
                docker rm portainer-app
                docker run -d -p "$PORT_PORTAINER":9443 --name portainer-app --restart=always \
                    -v /var/run/docker.sock:/var/run/docker.sock \
                    -v e-combox_portainer_portainer-data:/data \
                    -v /etc/localtime:/usr/share/zoneinfo/Europe/Paris:ro \
                    -e VIRTUAL_HOST=portainer \
                    -e VIRTUAL_PROTO=https \
                    -e VIRTUAL_PORT=9443 \
                    -e NODE_TLS_REJECT_UNAUTHORIZED=0 \
                    -e TZ="Europe/Paris" --network=bridge_e-combox \
                    "$image_utilitaire" \
                    --admin-password '$$2y$$05$$BZDHtzwzM1DEgL5pjAePAuoAbX6DknRE3XW3aU6HgBbwYdqwRcj3q' \
                    -H unix:///var/run/docker.sock \
                    --log-level=DEBUG

                if [ $? = 0 ]; then
                    echo -e "Lancement de Portainer... Succès"
                    echo -e "Portainer a été lancé.\n" >>"$LOGFILE"
                else
                    echo -e "$COLSTOP"
                    echo -e "Lancement de Portainer... Échec"
                    echo -e "$COLCMD"
                    echo -e "Consultez les log \"$LOGFILE\".\n"
                    echo -e "Portainer n'a pas pu être lancé." >>"$LOGFILE"
                    exit
                fi
            else
                echo -e "${COLSTOP}\nL'image $image_utilitaire n'existe pas${COLCMD}"
                echo -e "L'image $image_utilitaire n'existe pas" >>"$LOGFILE"
                exit
            fi
        else
            echo -e "${COLSTOP}\nIl n'est pas possible de supprimer cette image${COLCMD}"
            exit
        fi
        ;;
    *) echo -e "${COLSTOP}\nL'image $image_utilitaire n'est pas prise en charge${COLCMD}" ;;
    esac
fi

# Suppression de toutes les images
if [ -n "$delete" ] && [ -n "$full" ]; then
    # Suppression des images non associées à un site en cours d'exécution
    if [ -z "$type_o" ]; then
        for image in $(docker images --format "{{.Repository}}:{{.Tag}}"); do
            if (docker rmi "$image" >>"$LOGFILE" 2>&1); then
                echo -e "Suppression de $image... Fait"
            fi
        done
        echo -e "Images non associées à un site en exécution... Supprimées"

        # Suppression des images orphelines (image qui n'a pas de conteneur associé, mais qui est toujours référencée par l'image parente)
        if [ "$(docker images -qf dangling=true)" ]; then
            # shellcheck disable=SC2046
            docker rmi $(docker images -qf dangling=true) >>"$LOGFILE" 2>&1
            echo -e "Images orphelines... Supprimées"
        fi
    fi
fi

# Mise à jour de toutes les images des sites
if [ -n "$update" ] && [ -n "$full" ]; then
    for image in $(docker images --format "{{.Repository}}:{{.Tag}}"); do
        if ! [[ "$image" =~ e-combox || "$image" =~ docker-gen || "$image" =~ registry || "$image" =~ nginx || "$image" =~ portainer || "$image" =~ git ]]; then
            echo -e "Mise à jour de $image... En cours"
            docker pull "$image" >>"$LOGFILE" 2>&1
            echo -e "Mise à jour de $image... Fait"
        fi
    done
fi
