#!/bin/bash
# shellcheck disable=SC2034

# Définition de la branche, de la version du script, des applications et des URL vers les dépôts
BRANCHE="4.3"
VERSION_APPLI="4.3"
VERSION_SCRIPT="4.3.0"
VERSION_FICHIER_PARAM="4.3.0"
VERSION_PORTAINER="2.19.4"
TAG="4.3"
LOGFILE="/var/log/ecombox.log"
FORGE="forge.apps.education.fr"
DEPOT_GITLAB_SCRIPTS="https://$FORGE/e-combox/e-combox_scriptslinux"
DEPOT_GITLAB_PORTAINER="https://$FORGE/e-combox/e-combox_portainer"
DEPOT_GITLAB_RP_EXT="https://$FORGE/e-combox/e-combox_reverseproxy"
DEPOT_GITLAB_DC="https://$FORGE/e-combox/e-combox_docker-compose"

# Définition du dossier d'installation
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi

# Définition du dossier de migration
DOSSIER_MIGRATION="$DOSSIER_INSTALL/migration"

# Définition du dossier des certificats
DOSSIER_CERTS="$DOSSIER_INSTALL/certs"

# Définition du dossier de portainer
DOSSIER_PORTAINER=$DOSSIER_INSTALL/e-combox_portainer
# Le nom du dossier de Portainer a été modifié
if [ -d "$DOSSIER_INSTALL/e-comBox_portainer" ]; then
    mv "$DOSSIER_INSTALL/e-comBox_portainer" "$DOSSIER_PORTAINER"
fi

# Définition du dossier du reverse proxy
DOSSIER_RP=$DOSSIER_INSTALL/e-combox_reverseproxy
# Le nom du dossier du reverse-proxy a été modifié
if [ -d "$DOSSIER_INSTALL/e-comBox_reverseproxy" ]; then
    mv "$DOSSIER_INSTALL/e-comBox_reverseproxy" "$DOSSIER_RP"
fi

# Définition du fichier de paramètres
FICHIER_PARAM=$DOSSIER_INSTALL/param.conf

# Couleurs
COLTITRE="\033[1;35m"  # Rose
COLPARTIE="\033[1;34m" # Bleu
COLTXT="\033[0;37m"    # Gris
COLCHOIX="\033[1;33m"  # Jaune
COLDEFAUT="\033[0;33m" # Brun-jaune
COLSAISIE="\033[1;32m" # Vert
COLCMD="\033[1;37m"    # Blanc
COLSTOP="\033[1;31m"   # Rouge
COLINFO="\033[0;36m"   # Cyan
COLTERM="\033[0m"
