#!/bin/bash

# Définition de la branche et de la version des applications
BRANCHE="4.3"

# Récupération sur gitlab du fichier qui va effectivement configurer l'application
wget -q https://forge.apps.education.fr/e-combox/e-combox_scriptslinux/-/raw/$BRANCHE/start_configure_application.sh?ref_type=head --output-document start_configure_application.sh

# Exécution du fichier
bash start_configure_application.sh "$@"

# Suppression du fichier
rm start_configure_application.sh