#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

SHOW_USAGE() {
    echo -e "\nCe script permet la restauration d'un site (le nom du site doit être passé en option) ou de l'intégralité des sites."
    echo -e "Des options doivent être passées en ligne de commande (voir Usage ci-après)."
    echo -e "Usage: bash $0 -f|r \"nom du site\" [-h] [-s] [-a \"chemin_complet_archive_sauvegarde.tar.gz\"]"
    echo -e "\t-f\t\tRestauration de l'intégralité des sites."
    echo -e "\t-r\t\tRestauration d'un seul site [-r \"nom du site\"]"
    echo -e "\t-a\t\tChemin vers l'archive de sauvegarde [-a \"chemin_complet_archive_sauvegarde.tar.gz\"]". Si le chemin est fourni le script se déroulera sans interactivité.
    echo -e "\t-h\t\tDétail des options"
}

# Initialisation des paramètres passés au script
while getopts "fa:r:h" option; do
    # f: Restauration de l'intégralité des sites
    # r: Restauration d'un seul site
    # a: Chemin vers l'archive de sauvegarde
    # h: help

    case $option in
    f)
        if [ "$full_o" == "false" ]; then
            echo -e "${COLSTOP}\nErreur : vous ne pouvez pas utiliser les options -a et -r simultanément.${COLCMD}"
            SHOW_USAGE
            exit 1
        fi
        full_o="true"
        ;;
    r)
        if [ "$full_o" == "true" ]; then
            echo -e "${COLSTOP}\nErreur : vous ne pouvez pas utiliser les options -a et -r simultanément.${COLCMD}"
            SHOW_USAGE
            exit 1
        fi
        full_o="false"
        nom_site_o="$OPTARG"

        ;;

    a)
        archive_o="$OPTARG"
        echo "L'archive est $archive_o"
        ;;
    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        SHOW_USAGE
        exit 1
        ;;
    esac
done

# Vérification de la cohérence des options passées

# Pour forcer l'utilisation d'une des deux options "-f" ou "-r"
if [ -z "$full_o" ]; then
    echo -e "${COLSTOP}Une des deux options \"-f\" ou \"-r\" doit obligatoirement être utilisée\n${COLCMD}."
    SHOW_USAGE
    exit
fi

if [ -z "$archive_o" ]; then
    archive_o="false"
else
    # Test du chemin
    # Reconstitution des nom du dossier d'archivage
    DOSSIER_ARCHIVE=$(dirname "$ARCHIVE")
    if [ ! -d "$DOSSIER_ARCHIVE" ]; then
        echo -e "${COLSTOP}\nDossier $archive_o... Non trouvé${COLCMD}"
        exit 1
    fi
fi

if [ -z "$nom_site_o" ]; then
    nom_site_o="false"
fi

#echo $archive_o, $nom_site_o", "$full_o"

RESTAURE_SITES "$archive_o" "$nom_site_o"
