#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

SHOW_USAGE() {
    echo -e "\nCe script permet la mise à jour de la configuration de l'authentification."
    echo -e "Des options doivent être passées en ligne de commande (voir Usage ci-après)."
    echo -e "Usage: bash $0 -a|i [-h] [-s] [-j \"CLIENT_ID\"] [-k \"CLIENT_SECRET\"] [-l \"AUTHORIZATION_URL=\"] [-m \"ACCESS_TOKEN_URL=\"] [-n \"RESOURCE_URL=\"] [-o \"LOGOUT_URLL=\"] [-p \"USER_IDENTIFIER=\"] [-q \"SCOPES\"] [-r \"/CHEMIN/CERT_OAUTH\"]"
    echo -e "\t-a\t\tActivation de l'authentification 0Auth. Dans ce cas les autres paramètres doivent être renseignés soit dans param.conf,soit passés en option à l'exécution du script."
    echo -e "\t-i\t\tActivation de l'authentification Interne"
    echo -e "\t-s\t\tActivation du SS0"
    echo -e "\t-j\t\tIdentifiant public de l'application OAuth [-j \"CLIENT_ID\"]"
    echo -e "\t-k\t\tClient secret  [-k \"CLIENT_SECRET\"]"
    echo -e "\t-l\t\tURL pour s'authentifier auprès du fournisseur OAuth [-l \"AUTHORIZATION_URL=\"]"
    echo -e "\t-m\t\tURL pour récupérer le token d'accès [-m \"ACCESS_TOKEN_URL=\"]"
    echo -e "\t-n\t\tURL pour récupérer des informations sur l'utilisateur authentifié [-n \"RESOURCE_URL=\"]"
    echo -e "\t-o\t\tURL pour rediriger l'utilisateur vers le fournisseur OAuth afin de déconnecter l'utilisateur de la session [-o \"LOGOUT_URL=\"]"
    echo -e "\t-p\t\tIdentifiant qui sera utilisé par Portainer pour créer un compte pour l'utilisateur authentifié [-p \"USER_IDENTIFIER=\"]"
    echo -e "\t-q\t\tÉtendues requises par le fournisseur OAuth pour récupérer des informations sur l'utilisateur authentifié [-q \"SCOPES\"]"
    echo -e "\t-r\t\tFichier certificat du fournisseur 0Auth  [-r \"/CHEMIN/CERT_OAUTH\"]"
    echo -e "\t-h\t\tDétail des options"
}

# Initialisation des paramètres passés au script
while getopts "aihsj:k:l:m:n:o:p:q:r:" option; do
    # a: activation de l'authentification 0Auth
    # i: activation de l'authentification Interne
    # s: activation du SS0
    # j: identifiant public de l'application OAuth
    # k: client secret
    # l: URL pour s'authentifier auprès du fournisseur OAuth
    # m: URL pour récupérer le token d'accès
    # n: URL pour récupérer des informations sur l'utilisateur authentifié
    # o: URL pour rediriger l'utilisateur vers le fournisseur OAuth afin de déconnecter l'utilisateur de la session
    # p: identifiant qui sera utilisé par Portainer pour créer un compte pour l'utilisateur authentifié
    # q: étendues requises par le fournisseur OAuth pour récupérer des informations sur l'utilisateur authentifié
    # r: fichier certificat du fournisseur 0Auth
    # h: help

    case $option in
    a)
        if [ "$oauth_o" == "false" ]; then
            echo "Erreur : vous ne pouvez pas utiliser les options -a et -i simultanément."
            SHOW_USAGE
            exit 1
        fi
        oauth_o="true"
        sed -i "s|^OAUTH_ENABLE=.*|OAUTH_ENABLE=\"$oauth_o\"|" "$FICHIER_PARAM"
        ;;
    i)
        if [ "$oauth_o" == "true" ]; then
            echo "Erreur : vous ne pouvez pas utiliser les options -a et -i simultanément."
            SHOW_USAGE
            exit 1
        fi
        oauth_o="false"
        sed -i "s|^OAUTH_ENABLE=.*|OAUTH_ENABLE=\"$oauth_o\"|" "$FICHIER_PARAM"
        ;;
    s) sso_o="true" ;;
    j) client_id_o=$OPTARG ;;
    k) client_secret_o=$OPTARG ;;
    l) authorization_url_o=$OPTARG ;;
    m) access_token_url_o=$OPTARG ;;
    n) resource_url_o=$OPTARG ;;
    o) logout_url_o=$OPTARG ;;
    p) user_identifier_o=$OPTARG ;;
    q) scopes_o=$OPTARG ;;
    r) cert_oauth_o=$OPTARG ;;
    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        SHOW_USAGE
        exit 1
        ;;
    esac
done

# Vérification de la cohérence des options passées

# Pour forcer l'utilisation d'une des deux options "-a" ou "i"
if [ -z "$oauth_o" ]; then
    echo -e "${COLSTOP}Une des deux options \"-a\" ou \"-i\" doit obligatoirement être utilisée\n${COLCMD}."
    SHOW_USAGE
    exit
fi

# Si l'option "-a" n'est pas passée, il n'y a aucune raison de passer les autres options
if [ "$oauth_o" = "false" ] && [[ -n "$sso_o" || -n "$client_id_o" || -n "$client_secret_o" || -n "$authorization_url_o" || -n "$access_token_url_o" || -n "$resource_url_o" || -n "$logout_url_o" || -n "$user_identifier_o" || -n "$scopes_o" || -n "$cert_oauth_o" ]]; then
    echo -e "$COLSTOP"
    echo -e "Si vous voulez activez l'authentification OAuth, utilisez l'option \"-a\" sinon, avec l'option \"-i\", vous ne devez pas préciser d'options supplémentaires."
    echo -e "$COLCMD"
    SHOW_USAGE
    exit
fi

# Mise à jour du fichier param.conf en fonction des options passées
if [ -n "$sso_o" ]; then
    sed -i "s|^SSO_ENABLE=.*|SSO_ENABLE=\"$sso_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$client_id_o" ]; then
    sed -i "s|^CLIENT_ID=.*|CLIENT_ID=\"$client_id_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$client_secret_o" ]; then
    sed -i "s|^CLIENT_SECRET=.*|CLIENT_SECRET=\"$client_secret_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$authorization_url_o" ]; then
    sed -i "s|^AUTHORIZATION_URL=.*|AUTHORIZATION_URL=\"$authorization_url_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$access_token_url_o" ]; then
    sed -i "s|^ACCESS_TOKEN_URL=.*|ACCESS_TOKEN_URL=\"$access_token_url_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$resource_url_o" ]; then
    sed -i "s|^RESOURCE_URL=.*|RESOURCE_URL=\"$resource_url_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$logout_url_o" ]; then
    sed -i "s|^LOGOUT_URL=.*|LOGOUT_URL=\"$logout_url_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$user_identifier_o" ]; then
    sed -i "s|^USER_IDENTIFIER=.*|USER_IDENTIFIER=\"$user_identifier_o\"|" "$FICHIER_PARAM"
fi

if [ -n "$scopes_o" ]; then
    sed -i "s|^SCOPES=.*|SCOPES=\"$scopes_o\"|" "$FICHIER_PARAM"
fi
if [ -n "$cert_oauth_o" ]; then
    sed -i "s|^CERT_OAUTH=.*|CERT_OAUTH=\"$cert_oauth_o\"|" "$FICHIER_PARAM"
fi

# On recharge le fichier param.conf
source "$FICHIER_PARAM"

# Configuration de l'authentification
CONFIGURE_OAUTH