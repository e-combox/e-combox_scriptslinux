#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

SHOW_USAGE() {
    echo -e "$COLCMD"
    echo -e "\nCe script permet de supprimer l'application e-comBox."
    echo -e "Attention, le dossier /opt/e-combox sera supprimé ainsi que les logs."
    echo -e "Une option -e ou -a doit obligatoirement être passée en ligne de commande (voir Usage ci-après)."
    echo -e "Une option -f supplémentaire doit être passée en cas de problème d'accès à Portainer"
    echo -e "Usage: bash $0 -e | -a -f [-h]"
    echo -e "\t-e\t\tSuppression de l'application uniquement"
    echo -e "\t-a\t\tSuppression de l'application, docker et docker-compose"
    echo -e "\t-f\t\tForce la suppression même si l'accès à Portainer n'est plus possible"
    echo -e "\t-h\t\tDétail des options"
}

# Initialisation des paramètres passés au script.
while getopts "aefh" option; do
    # a: suppression de docker et docker-compose
    # e: suppression de l'application uniquement
    # f: force la suppression de l'application même si Portainer n'est plus accessible

    case $option in
    a) docker="true" ;;
    e) application="true" ;;
    f) del_stop_stack="true" ;;
    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        exit 1
        ;;
    esac
done

# Vérification des données
if [ -z "$docker" ] && [ -z "$application" ]; then
    echo -e "${COLSTOP}Une des deux options \"-a\" ou \"-e\" doit obligatoirement être utilisée\n${COLCMD}."
    SHOW_USAGE
    exit 1
fi

# Suppression de l'application

# Arrêt des stacks et conteneurs
if (docker ps -a >>"$LOGFILE" 2>&1); then
    echo -e "Arrêt des sites... En cours"
    if (docker ps | grep "portainer-app" >>"$LOGFILE" 2>&1); then
        if [ ! "$del_stop_stack" = "true" ]; then
            STOP_STACKS
        fi
    fi
    # shellcheck disable=SC2046
    docker stop $(docker ps -aq) >>"$LOGFILE" 2>&1
    # Redémarrage du démon
    systemctl daemon-reload
fi

# Suppression des conteneurs, volumes, réseaux images
echo -e "\nConteneurs, volumes, réseaux et images... En cours"
docker system prune -a -f --volumes >>"$LOGFILE" 2>&1

# Suppression des volumes (dans certains cas, les volumes ne se suppriment pas avec la commande précédente)
#shellcheck disable=SC2046
if (docker volume ls --format "{{.Name}}" >>"$LOGFILE" 2>&1); then
    # Debug
    # echo -e "\nSuppression des volumes qui ne se sont pas supprimés avec docker prune."
    echo -e "\nSuppression des volumes qui ne se sont pas supprimés avec docker prune." >>"$LOGFILE"
    docker volume rm $(docker volume ls --format "{{.Name}}") >>"$LOGFILE" 2>&1
fi

echo -e "\nConteneurs, volumes, réseaux et images... Supprimés"
echo -e "\nConteneurs, volumes, réseaux et images... Supprimés" >>"$LOGFILE"

echo -e "$COLINFO"
echo -e "Suppression de l'application e-comBox... Succès."
echo -e "$COLCMD"

# Suppression des fichiers et dossiers spécifiques
rm /var/log/ecombox*
echo -e "Journaux... Supprimés"
if [ -e /etc/cron.daily/cron_certificats.sh ]; then
    rm -f /etc/cron.daily/cron_certificat.sh
    echo -e "Suppression du renouvellement automatique des certificats... Fait"
fi
rm -rf "$DOSSIER_INSTALL"
echo -e "$DOSSIER_INSTALL... Supprimé"

# Suppression de docker et docker-compose
if [ "$docker" = "true" ]; then
    # Suppression du fichier de lancement éventuellement créé
    if [ -d /etc/systemd/system/docker.service.d/ ]; then
        rm -rf /etc/systemd/system/docker.service.d
    fi
    # Arrêt de docker
    systemctl stop docker
    systemctl stop docker.socket
    # Suppression des paquets
    apt-get purge -y docker-ce docker-ce-cli docker-ce-rootless-extras docker-compose-plugin docker-scan-plugin
    # Suppression des binaires
    rm -rf "$(which docker)"
    rm -rf "$(which docker-compose)"
    # Redémarrage du démon
    systemctl daemon-reload
    # Suppression de /etc/docker
    rm -rf /etc/docker
    echo -e "Suppression manuelle de /etc/docker... Fait"
    # Suppression de /var/lib/docker
    rm -rf /var/lib/docker
    echo -e "Suppression manuelle de /var/lib/docker... Fait"

    echo -e "$COLINFO"
    echo -e "Suppression de Docker et Docker-compose... Succès."
    echo -e "$COLCMD"
fi
