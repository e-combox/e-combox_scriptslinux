#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
  mv /opt/e-comBox $DOSSIER_INSTALL
fi

# Appel des fichiers de variables et de paramètres
source $DOSSIER_INSTALL/variables.sh
#source ./variables.sh
#source "$FICHIER_PARAM"
#source ./param.conf

# Chiffre le mot de passe de Portainer écrit en clair
PROTECT_MDP() {

  if [ ! -e "$DOSSIER_MDP_KEY"/secret_key ]; then
    # Génération de la clé de chiffrement et de déchiffrement si elle n'existe pas déjà
    echo $RANDOM | md5sum | head -c 20 >"$DOSSIER_MDP_KEY"/secret_key
    chmod 600 "$DOSSIER_MDP_KEY"/secret_key
    echo -e "Création et protection de la clé de chiffrement... Fait"
    echo -e "\nLa clé de chiffrement a été créée et protégée." >>"$LOGFILE"
  else
    echo -e "Clé de chiffrement... Présente."
    echo -e "\nLa clé de chiffrement existe déjà." >>"$LOGFILE"
  fi

  # S'il existe un mdp en clair dans le fichier, le script le chiffre dans un fichier et le supprime
  if [ -n "$MDP_PORTAINER" ]; then

    # Chiffrement du mot de passe de Portainer
    echo -e "Chiffrement du mot de passe de Portainer... Fait"
    echo -e "Chiffrement du mot de passe de Portainer.\n" >>"$LOGFILE"
    KEY=$(cat "$DOSSIER_MDP_KEY"/secret_key)
    #echo "la clé est $KEY".
    echo "$MDP_PORTAINER" | openssl enc -aes-256-cbc -k "$KEY" -pbkdf2 -e >"$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt
    chmod 600 "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt
    # Debug
    #MDP_PORTAINER_CHIFFRE=$(cat "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt)
    #echo Le mot de passe chiffré est "$MDP_PORTAINER_CHIFFRE"

    # Supprime le mot de passe en clair
    sed -i "s|^MDP_PORTAINER=.*|MDP_PORTAINER=\"\"|" "$FICHIER_PARAM"

  # Affiche si un mot de passe chiffré a bien été trouvé
  elif [ -e "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt ]; then
    echo -e "Mot de passe chiffré... Trouvé"
    echo -e "Le mot de passe chiffré existe.\n" >>"$LOGFILE"
  fi

}

# Teste si la valeur est une adresse IP ou un nom de domaine valide et si cela correspond à une adresse IP privée ou publique
TEST_IP_DOMAINE() {
  IP_DOMAINE=$1
  # On vérifie si la valeur est un nom de domaine ou une adresse IP
  if [[ "$IP_DOMAINE" =~ [A-Za-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[A-Za-z]{2,} ]]; then
    echo -e "C'est un nom de domaine valide.\n" >>"$LOGFILE"
    valide_domaine="true"
    # Il s'agit d'un nom de domaine, on tente une résolution
    # Installation de bind9-dnsutils (pour la commande nslookup) si besoin
    if ! (nslookup "$IP_DOMAINE" >/dev/null 2>&1); then
      echo -e "Commande nslookup... Absente"
      echo -e "Téléchargement et installation de bind9-dnsutils... En cours"
      apt-get update >>"$LOGFILE" 2>&1
      if (apt-get install -y bind9-dnsutils >>"$LOGFILE" 2>&1); then
        echo -e "Installation de bind9-dnsutils... Fait"
      else
        echo -e "Installation de bind9-dnsutils... Paquet non trouvé"
        v_nslookup="false"
      fi
    fi
    if (nslookup "$IP_DOMAINE" | grep "server can't find" >/dev/null) || (nslookup "$IP_DOMAINE" | grep "no servers could be reached" >/dev/null) || [ "$v_nslookup" = "false" ]; then
      # La résolution ne peut pas se faire
      echo -e "La résolution de nom a échoué." >>"$LOGFILE"
      resolution_nom_domaine="false"
    else
      IP_A_TESTER=$(nslookup "$IP_DOMAINE" | grep "Address" | tail -n 1 | awk '{print $2}')
      if [ -n "$IP_A_TESTER" ]; then
        resolution_nom_domaine="true"
        echo -e "Le nom de domaine a été résolu en $IP_A_TESTER" >>"$LOGFILE"
      else
        resolution_nom_domaine="false"
        echo -e "Le nom de domaine n'a pas pu être résolu" >>"$LOGFILE"
      fi
    fi
  else
    IP_A_TESTER="$IP_DOMAINE"
    echo "l'IP à tester est $IP_A_TESTER" >>"$LOGFILE"

  fi
  IFS='.' read -ra octets <<<"$IP_A_TESTER"
  if [ ${#octets[@]} -eq 4 ]; then
    # L'adresse IP contient bien 4 octets
    valide_ip="true"
    for octet in "${octets[@]}"; do
      if ! [[ $octet =~ ^[0-9]+$ ]] || [[ $octet -gt 255 ]]; then
        # Un des octets n'est pas compris entre 0 et 255
        valide_ip="false"
        break
      fi
    done
    if [ "$valide_ip" = "true" ]; then
      echo -e "L'adresse IP est valide.\n" >>"$LOGFILE"
      # On détermine s'il s'agit d'une adresse IP privée ou d'une adresse IP publique
      # Teste si l'adresse IP appartient à une des plages privées
      if [[ ${octets[0]} -eq 10 ]] || [[ ${octets[0]} -eq 127 ]] || [[ ${octets[0]} -eq 172 && ${octets[1]} -ge 16 && ${octets[1]} -le 31 ]] || [[ ${octets[0]} -eq 192 && ${octets[1]} -eq 168 ]]; then
        valide_ip_privee="true"
        valide_ip_publique="false"
        # On écrit dans les logs s'il s'agit d'une interface de loopback
        if [[ ${octets[0]} -eq 127 ]]; then
          echo -e "Attention, la valeur correspond à une interface de loopback.\n" >>"$LOGFILE"
        fi
      else
        valide_ip_privee="false"
        valide_ip_publique="true"
      fi
    else
      echo -e "L'adresse IP n'est pas valide.\n" >>"$LOGFILE"
    fi
  else
    if [ "$resolution_nom_domaine" = "false" ]; then
      echo -e "\nLa résolution du domaine a échoué."
      echo -e "\nLa résolution du domaine a échoué." >>"$LOGFILE"
    else
      echo -e "L'adresse IP n'est pas valide car elle ne contient pas 4 octets.\n" >>"$LOGFILE"
    fi
  fi
}

BACKUP() {
  # Chemin complet vers le fichier
  FICHIER=$1

  if [ -e "${FICHIER}".3 ]; then
    cp "${FICHIER}".3 "${FICHIER}".4
    chmod 600 "${FICHIER}".4
  fi

  if [ -e "${FICHIER}".2 ]; then
    cp "${FICHIER}".2 "${FICHIER}".3
    chmod 600 "${FICHIER}".3
  fi

  if [ -e "${FICHIER}".1 ]; then
    cp "${FICHIER}".1 "${FICHIER}".2
    chmod 600 $"${FICHIER}".2
  fi

  if [ -e "${FICHIER}" ]; then
    cp "$FICHIER" "${FICHIER}".1
    chmod 600 "${FICHIER}".1
  fi
}

VERIF_MDP_PORTAINER() {
  PASS_PORTAINER="$1"
  REINSTALL="$2"

  # Debug
  #echo "Fonction VERIF_MDP_PORTAINER : $PASS_PORTAINER"
  #echo "VARIABLE REINSTALL : $REINSTALL"

  if [ -z "${PASS_PORTAINER:11}" ]; then
    echo -e "$COLSTOP"
    # Caractères testés param.conf : " $ \ { } ' / . , [ ] | ^ % ( ) & > = # ~ + ` * [space]
    # Caractères testés en ligne de commande : " $ \ { } ' / . , [ ] | ^ % ( ) & > = # ~ ` + ` * [space]
    # Caractères qui ne passent pas en ligne de commande : " $ \ & | ` [space]
    # Caractères qu'il faut échapper dans le fichier param.conf : " $ `
    echo -e "Le mot de passe doit faire plus de 12 caractères. Merci de le modifier."
    echo -e "$COLINFO"
    echo -e "Le mot de passe renseigné ne supporte pas les caractères spéciaux \" / \$ \`"
    echo -e "Le mot de passe ne peut pas être égal à portnairAdmin qui est un mot de passe connu de tous." >>"$LOGFILE"
    echo -e "$COLCMD"
    echo -e "Le mot de passe doit faire plus de 12 caractères." >>"$LOGFILE"
    echo -e "$COLCMD"
    mdp_p="false"

  elif (echo "$PASS_PORTAINER" | grep "\"" >/dev/null) || (echo "$PASS_PORTAINER" | grep "\\$" >/dev/null) || (echo "$PASS_PORTAINER" | grep "\`" >/dev/null); then
    # Le mot de passe ne doit pas contenir les caractères spéciaux ", et $
    echo -e "$COLSTOP"
    echo -e "Le mot de passe ne doit pas contenir les caractères spéciaux \" \$. Merci de le modifier."
    echo -e "Le mot de passe ne doit pas contenir les caractères spéciaux \" \$." >>"$LOGFILE"
    echo -e "$COLCMD"
    mdp_p="false"

  elif [ "$PASS_PORTAINER" = "portnairAdmin" ]; then
    echo -e "$COLSTOP"
    echo -e "Le mot de passe renseigné ne peut pas être égal à portnairAdmin qui est un mot de passe connu de tous."
    echo -e "$COLINFO"
    echo -e "Le mot de passe renseigné ne supporte pas les caractères spéciaux \' \" \$ \{ \}"
    echo -e "Le mot de passe ne peut pas être égal à portnairAdmin qui est un mot de passe connu de tous." >>"$LOGFILE"
    echo -e "$COLCMD"
    mdp_p="false"

    # Debug
    #portainer=$(docker ps | grep portainer-app)
    #echo "VARIAB PORTAINER : $portainer"
    #echo "VARIABLE REINSTALL : $REINSTALL"

  elif (docker ps | grep portainer-app >/dev/null) && [ "$REINSTALL" = "false" ]; then
    # On vérifie que le mot de passe est identique à celui de Portainer (si ce dernier n'est pas celui par défaut)
    # Debug
    #echo "On est dans la vérification du mdp de portainer"
    # Récupération des identifiants pour se connecter à l'API de Portainer
    P_USER="admin"
    P_URL="https://localhost:$PORT_PORTAINER"
    P_PRUNE="false"
    P_PASS_DEFAUT=portnairAdmin

    # Portainer utilise t-il encore le mdp par défaut (ce qui est le cas si nouvelle installation)
    TEST_MDP_DEFAUT=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"admin\",\"password\":\"$P_PASS_DEFAUT\"}" "$P_URL/api/auth")

    if (echo "$TEST_MDP_DEFAUT" | grep "Invalid credentials" &>/dev/null); then
      #Échappement éventuel du mdp
      P_PASS=${PASS_PORTAINER/\\/\\\\\\\\}
      P_PASS=${P_PASS/\"/\\\\\\\"}
      P_PASS=${P_PASS/\$/\\\$}

      TEST_MDP_PORTAINER=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"admin\",\"password\":\"$P_PASS\"}" "$P_URL/api/auth")
      if (echo "$TEST_MDP_PORTAINER" | grep "Invalid credentials" &>/dev/null); then
        echo -e "$COLSTOP"
        echo -e "Impossible d'accéder à l'API de Portainer. Le mot de passe $TEST_MDP_PORTAINER est différent de celui de Portainer."
        echo -e "Impossible d'accéder à l'API de Portainer. Le mot de passe est différent de celui de Portainer." >>"$LOGFILE"
        echo -e "$COLCMD"
        mdp_p="false"
      else
        mdp_p="true"
      fi
    else
      mdp_p="true"
    fi
  else
    mdp_p="true"
    # Debug
    #echo -e "Vérif que le MDP de Portainer est identique à celui de Portainer : OK."
  fi

}

VERIF_PARAM() {

  echo -e "Vérification des paramètres du fichier param.conf\n" >>"$LOGFILE"
  echo -e "Vérification du fichier param.conf... En cours"
  # Vérification de la version du fichier
  if [ ! "$VERSION_PARAM" = "$VERSION_FICHIER_PARAM" ]; then
    echo -e "La version du fichier de paramètres est différente\n" >>"$LOGFILE"
    sed -i "s|^VERSION_PARAM=.*|VERSION_PARAM=\"$VERSION_FICHIER_PARAM\"|" "$FICHIER_PARAM"
    # Sauvegarde du fichier param.conf en param.old.conf et protection en lecture
    cp "$FICHIER_PARAM" $DOSSIER_INSTALL/param.old.conf
    chmod 600 $DOSSIER_INSTALL/param.old.conf

    # Téléchargement du fichier param.conf par défaut
    curl -fsSL "$DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/param.conf" -o "$FICHIER_PARAM"
    chmod 600 "$FICHIER_PARAM"

    # Récupération des variables et des valeurs dans param.old.conf
    # Et traitement de l'ancien paramètre "ADRESSE_IP" de la version 3
    while read -r line; do
      # On vérifie que la ligne n'est ni vide ni un commentaire
      if [[ $line != "" && ${line:0:1} != "#" ]]; then
        # On stocke le nom de la variable dans variable_nom
        variable_nom=$(echo "$line" | cut -d'=' -f1)
        # On stocke la valeur de la variable dans variable_valeur
        variable_valeur=$(echo "$line" | cut -d'=' -f2 | tr -d "\"")
        # Traitement de l'ancien paramètre "ADRESSE_IP" de la version 3
        if [ "$variable_nom" = "ADRESSE_IP" ]; then
          # Il s'agit de l'ancien fichier, il faut soit placer le contenu dans un des deux premiers paramètres
          echo -e "Il s'agit d'un fichier de paramètre de la version 3\n" >>"$LOGFILE"
          # Si une variable "ADRESSE_IP_PRIVEE" n'est pas présente
          if ! (cat <"$DOSSIER_INSTALL/param.old.conf" | grep "ADRESSE_IP_PRIVEE" >/dev/null); then
            TEST_IP_DOMAINE "$variable_valeur"
            if [ "$resolution_nom_domaine" = "false" ]; then
              # La résolution ne peut pas se faire, tant pis on estime que c'est un nom DNS local
              echo -e "La résolution de nom a échoué.\n" >>"$LOGFILE"
              variable_nom="ADRESSE_IP_PRIVEE"
            fi
            # Si la valeur est une adresse IP privée, elle sera affectée à ADRESSE_IP_PRIVEE
            # Sinon, elle sera affectée à DOMAINE
            if [ "$valide_ip_privee" = "true" ]; then
              variable_nom="ADRESSE_IP_PRIVEE"
            else
              variable_nom="DOMAINE"
            fi
          fi
        fi

        # On traite les variables nulles et sans guillemets
        if [ -n "$variable_valeur" ]; then
          # On ajoute les guillemets s'il n'y en a pas déjà
          # Debug
          #echo "Variables avant ajout de guillemets : $variable_valeur"
          if (! echo "$variable_valeur" | grep "\"" >/dev/null); then
            variable_valeur="\"$variable_valeur\""
            # Debug
            #echo "Variables après ajout de guillemets : $variable_valeur"
          fi

          # Mise à jour des valeurs dans param.conf
          #echo "$variable_nom"="$variable_valeur"
          variable_valeur=${variable_valeur/|/\|}
          sed -i "s|^$variable_nom=.*|$variable_nom=$variable_valeur|" "$FICHIER_PARAM"
        fi
      fi
    done <$DOSSIER_INSTALL/param.old.conf
  fi

  # Le dossier /opt/e-comBox a été renommé /opt/e-combox
  sed -i "s|/opt/e-comBox|/opt/e-combox|g" "$FICHIER_PARAM"

  # On recharge le fichier de paramètres
  source "$FICHIER_PARAM"

  #Vérification des valeurs saisies et sortie éventuelle du script
  t_variables_nom=(VALIDATION_LICENCE MDP_PORTAINER ADRESSE_IP_PRIVEE DOMAINE RP_EXT CHEMIN DOSSIER_MDP_KEY DOSSIER_MDP_CHIFFRE PORT_PORTAINER PORT_RP PORT_REGISTRY ADRESSE_PROXY NO_PROXY NET_ECB DEL_IMAGES CHEMIN_CERT CHEMIN_KEY MAIL CODE_PAYS NOM_PAYS NOM_REGION NOM_ORGANISATION OAUTH_ENABLE SSO_ENABLE CLIENT_ID CLIENT_SECRET AUTHORIZATION_URL ACCESS_TOKEN_URL RESOURCE_URL LOGOUT_URL USER_IDENTIFIER SCOPES CERT_OAUTH PROPRIETAIRE ADRESSE_POSTALE IMMATRICULATION HEBERGEUR)
  t_variables_valeur=("$VALIDATION_LICENCE" "$MDP_PORTAINER" "$ADRESSE_IP_PRIVEE" "$DOMAINE" "$RP_EXT" "$CHEMIN" "$DOSSIER_MDP_KEY" "$DOSSIER_MDP_CHIFFRE" "$PORT_PORTAINER" "$PORT_RP" "$PORT_REGISTRY" "$ADRESSE_PROXY" "$NO_PROXY" "$NET_ECB" "$DEL_IMAGES" "$CHEMIN_CERT" "$CHEMIN_KEY" "$MAIL" "$CODE_PAYS" "$NOM_PAYS" "$NOM_REGION" "$NOM_ORGANISATION" "$OAUTH_ENABLE" "$SSO_ENABLE" "$CLIENT_ID" "$CLIENT_SECRET" "$AUTHORIZATION_URL" "$ACCESS_TOKEN_URL" "$RESOURCE_URL" "$LOGOUT_URL" "$USER_IDENTIFIER" "$SCOPES" "$CERT_OAUTH" "$PROPRIETAIRE" "$ADRESSE_POSTALE" "$IMMATRICULATION" "$HEBERGEUR")
  t_variables_fausses=()

  # Pour chacun des paramètres
  for ((v = 0; v < ${#t_variables_nom[@]}; v++)); do
    # On stocke les variables utilisées sauf les valeurs correspondantes au mdp de Portainer
    if [ ! "${t_variables_nom[v]}" = MDP_PORTAINER ] && [[ ! "${t_variables_nom[v]}" =~ ^DOSSIER ]]; then
      echo "${t_variables_nom[v]}"="${t_variables_valeur[v]}" >>"$LOGFILE"
    fi

    # Vérification de la licence
    if [ "${t_variables_nom[v]}" = "VALIDATION_LICENCE" ]; then
      if [ "${t_variables_valeur[v]}" != "true" ] && [ "${t_variables_valeur[v]}" != "True" ]; then
        echo -e "$COLSTOP"
        echo -e "La licence de l'e-comBox n'est pas acceptée, l'installation ne peut pas continuer\n.Mettez \"true\" au paramètre VALIDATION_LICENCE et relancez le script configure_application.sh."
        echo -e "La licence de l'e-comBox n'est pas acceptée, l'installation ne peut pas continuer\n.Mettez \"true\" au paramètre VALIDATION_LICENCE et relancez le script configure_application.sh." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      fi
    fi

    # Vérification du mot de passe de Portainer

    # S'il existe un mot de passe en clair, il faut le vérifier et le chiffrer
    # Sinon, il faut vérifier que le mot de passe chiffré est accessible
    if [ "${t_variables_nom[v]}" = "MDP_PORTAINER" ]; then
      if [ -n "${t_variables_valeur[v]}" ]; then
        # Debug
        #echo -e "t_variable_valeur : ${t_variables_valeur[v]}"

        # Vérification du mot de passe de Portainer
        reinstall="false"
        VERIF_MDP_PORTAINER "${t_variables_valeur[v]}" "$reinstall"
        if [ ! "$mdp_p" = "false" ]; then
          # Chiffrement du mdp de Portainer si mot de passe valide
          PROTECT_MDP
        else
          t_variables_fausses+=("${t_variables_nom[v]}")
        fi
      else
        if [ ! -e "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt ]; then
          # Debug
          #existe=$(ls -la "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt)
          #echo -e Le fichier du mot de passe chiffré est "$existe".
          echo -e "$COLSTOP"
          echo -e "Aucun mot de passe en clair ou chiffré n'est trouvé. Veuillez saisir le mot de passe dans $FICHIER_PARAM ou vérifier l'accessibilité du mot de passe chiffré."
          echo -e "Aucun mot de passe en clair ou chiffré n'est trouvé." >>"$LOGFILE"
          echo -e "$COLCMD"
          t_variables_fausses+=("${t_variables_nom[v]}")
        fi
      fi
    fi

    # Vérification du paramètre ADRESSE_IP_PRIVEE
    if [ "${t_variables_nom[v]}" = "ADRESSE_IP_PRIVEE" ]; then
      if [ -z "${t_variables_valeur[v]}" ]; then
        echo -e "$COLSTOP"
        echo -e "L'adresse IP privée doit obligatoirement être renseignée sous forme d'adresse IP ou de nom DNS local.\nCe dernier doit pouvoir être résolu."
        echo -e "Adresse IP privée non renseignée." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      else
        TEST_IP_DOMAINE "${t_variables_valeur[v]}"
        if [ "$valide_ip" = "false" ]; then
          echo -e "$COLSTOP"
          echo -e "La valeur saisie au niveau de l'adresse IP privée n'est pas valide."
          echo -e "La valeur saisie au niveau de l'adresse IP privée n'est pas valide." >>"$LOGFILE"
          echo -e "$COLCMD"
          t_variables_fausses+=("${t_variables_nom[v]}")
        fi
        if [ "$valide_ip_privee" = "false" ]; then
          echo -e "$COLSTOP"
          echo -e "La valeur saisie au niveau de l'adresse IP privée ne correspond pas à une adresse IP privée. Cette valeur doit éventuellement être saisie au niveau du paramètre DOMAINE."
          echo -e "L'adresse IP saisie dans ADRESSE_IP_PRIVEE ne correspond pas à une adresse IP privée." >>"$LOGFILE"
          echo -e "$COLCMD"
          t_variables_fausses+=("${t_variables_nom[v]}")
        fi
      fi
    fi

    # Vérification du paramètre DOMAINE
    if [ "${t_variables_nom[v]}" = "DOMAINE" ]; then
      if [ -n "${t_variables_valeur[v]}" ]; then
        TEST_IP_DOMAINE "${t_variables_valeur[v]}"
        if [ "$valide_domaine" = "false" ] || [ "$valide_ip" = "false" ]; then
          echo -e "$COLSTOP"
          echo -e "La valeur saisie au niveau du nom de domaine n'est pas valide."
          echo -e "La valeur saisie au niveau du nom de domaine n'est pas valide." >>"$LOGFILE"
          echo -e "$COLCMD"
          t_variables_fausses+=("${t_variables_nom[v]}")
        else
          echo -e "La valeur saisie au niveau du nom de domaine est valide." >>"$LOGFILE"
          # La valeur saisie devrait correspondre à une adresse IP publique.
          # Mais une résolution interne pourrait faire que ce n'est pas le cas. On prévient juste...
          if [ ! "$valide_ip_publique" = "true" ]; then
            echo -e "$COLINFO"
            #echo -e "La valeur saisie au niveau du nom de domaine ne correspond pas (en interne) à une adresse IP publique."
            echo -e "La valeur saisie au niveau du nom de domaine ne correspond pas (en interne) à une adresse IP publique." >>"$LOGFILE"
            echo -e "$COLCMD"
          fi
          # On écrit également dans les log si la résolution du nom ne peut pas se faire mais on ne bloque pas
          if [ "$resolution_nom_domaine" = "false" ]; then
            echo -e "$COLSTOP"
            echo -e "Attention, la résolution du nom de domaine n'a pas pu se faire." >>"$LOGFILE"
            echo -e "$COLCMD"
          fi
        fi
      else
        echo -e "Pas de domaine saisi.\n" >>"$LOGFILE"
      fi
    fi

    # Vérification de la valeur de RP_EXT
    if [ "${t_variables_nom[v]}" = "RP_EXT" ]; then
      if ! [[ "${t_variables_valeur[v]}" = "O" || "${t_variables_valeur[v]}" = "o" ]] && ! [[ "${t_variables_valeur[v]}" = "N" || "${t_variables_valeur[v]}" = "n" ]]; then
        echo -e "$COLSTOP"
        echo -e "Le paramètre ${t_variables_nom[v]} ne peut contenir que les valeurs \"O\" ou \"N\"."
        echo -e "Le paramètre ${t_variables_nom[v]} ne peut contenir que les valeurs \"O\" ou \"N\"." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      elif [[ "${t_variables_valeur[v]}" = "O" || "${t_variables_valeur[v]}" = "o" ]] && [ -z "$DOMAINE" ]; then
        echo -e "$COLSTOP"
        echo -e "Le paramètre ${t_variables_nom[v]} contient la valeur \"O\". Veuillez renseigner une valeur pour le paramètre DOMAINE."
        echo -e "Le paramètre ${t_variables_nom[v]} contient la valeur \"O\", or le paramètre DOMAINE est vide." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("DOMAINE")
      elif [[ "${t_variables_valeur[v]}" = "N" || "${t_variables_valeur[v]}" = "n" ]] && [ -n "$CHEMIN" ]; then
        echo -e "$COLSTOP"
        echo -e "Le paramètre ${t_variables_nom[v]} contient la valeur \"N\". Un chemin est pourtant renseigné. Veuillez modifiez une des deux valeurs."
        echo -e "Le paramètre ${t_variables_nom[v]} contient la valeur \"N\", or le chemin est renseigné." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("CHEMIN OU RP_EXT")
      fi
    fi

    # Vérification de la syntaxe du chemin
    if [ "${t_variables_nom[v]}" = "CHEMIN" ]; then
      if (echo "${t_variables_valeur[v]}" | grep "/" >/dev/null); then
        echo -e "$COLSTOP"
        echo -e "Le paramètre ${t_variables_nom[v]} ne peut pas contenir de barres obliques (/)."
        echo -e "Le paramètre ${t_variables_nom[v]} ne peut pas contenir de barres obliques (/)." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      fi
    fi

    # Vérification des paramètres de type dossier
    if [[ "${t_variables_nom[v]}" =~ ^DOSSIER ]]; then
      if [ ! -d "${t_variables_valeur[v]}" ]; then
        echo -e "$COLSTOP"
        echo -e "Le dossier ${t_variables_valeur[v]} n'est pas accessible. Veuillez ajuster le paramètre ${t_variables_nom[v]}."
        echo -e "Le dossier ${t_variables_valeur[v]} n'est pas accessible." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      fi
    fi

    # Vérification du paramètre MAIL
    if [ "${t_variables_nom[v]}" = "MAIL" ]; then
      if [ -n "${t_variables_valeur[v]}" ]; then
        if [[ "${t_variables_valeur[v]}" =~ [a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}] ]]; then
          echo -e "$COLSTOP"
          echo -e "La valeur saisie au niveau de l'adresse mail n'est pas valide."
          echo -e "La valeur saisie au niveau de l'adresse mail n'est pas valide." >>/var/log/ecombox.log
          echo -e "$COLCMD"
          t_variables_fausses+=("${t_variables_nom[v]}")
        else
          echo -e "La valeur saisie au niveau de l'adresse mail est valide." >>/var/log/ecombox.log
        fi
      else
        echo -e "Pas d'adresse mail saisi.\n" >>/var/log/ecombox.log
      fi
    fi

    # Vérification que les 4 variables indispensables pour le certificat ne sont pas vides
    if [ "${t_variables_nom[v]}" = "CODE_PAYS" ] || [ "${t_variables_nom[v]}" = "NOM_PAYS" ] || [ "${t_variables_nom[v]}" = "NOM_REGION" ] || [ "${t_variables_nom[v]}" = "NOM_ORGANISATION" ]; then
      if [ -z "${t_variables_valeur[v]}" ]; then
        echo -e "$COLSTOP"
        echo -e "Le paramètre ${t_variables_nom[v]} doit être renseigné."
        echo -e "Le paramètre ${t_variables_nom[v]} doit être renseigné." >>"$LOGFILE"
        echo -e "$COLCMD"
        t_variables_fausses+=("${t_variables_nom[v]}")
      fi
    fi

    # Vérification du paramétrage de l'authentification basée sur Oauth
    # À faire

    # Vérification des variables relatives aux mentions légales
    # À faire

  done

  if [ ${#t_variables_fausses} -gt 0 ]; then
    echo -e "$COLSTOP"
    echo -e "La valeur des variables ${COLINFO}${t_variables_fausses[*]}${COLSTOP} de votre fichier $FICHIER_PARAM doit être corrigée."
    echo -e "La valeur des variables ${COLINFO}${t_variables_fausses[*]}${COLSTOP} du fichier $FICHIER_PARAM est fausse." >>"$LOGFILE"
    echo -e "Corrigez les erreurs et relancez le script /opt/e-combox/configure_application.sh"
    echo -e "$COLCMD"
    exit 1
  else
    echo -e "Vérification du fichier param.conf... Fait$COLCMD"
    echo -e "Fichier de paramètres sans erreurs apparentes." >>"$LOGFILE"
  fi

}

CONNECTE_API() {
  # Récupération des identifiants pour se connecter à l'API de Portainer
  P_USER="admin"
  P_URL="https://localhost:$PORT_PORTAINER"
  P_PRUNE="false"
  P_PASS_DEFAUT=portnairAdmin

  # Déchiffrement du mot de passe
  KEY=$(cat "$DOSSIER_MDP_KEY"/secret_key)
  MDP_PORTAINER=$(openssl enc -aes-256-cbc -k "$KEY" -pbkdf2 -d -in "$DOSSIER_MDP_CHIFFRE"/.mdp_portainer_chiffre.txt)
  # Debug
  #echo "Le mot de passe de portainer est $MDP_PORTAINER"

  #Échappement éventuel du mdp
  P_PASS=${MDP_PORTAINER/\\/\\\\\\\\}
  P_PASS=${P_PASS/\"/\\\\\\\"}
  P_PASS=${P_PASS/\$/\\\$}

  # Debug
  #echo "Le mot de passe après échappement est $P_PASS"

  # Portainer utilise t-il encore le mdp par défaut (ce qui est le cas si nouvelle installation)
  TEST_MDP_PORTAINER=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"admin\",\"password\":\"$P_PASS_DEFAUT\"}" "$P_URL/api/auth")
  echo -e "Test mdp par défaut de portainer : $TEST_MDP_PORTAINER" >>"$LOGFILE"
  # Debug
  #echo -e "Test mdp par défaut de portainer : $TEST_MDP_PORTAINER"

  # Si le test précédent renvoie un "Invalid credential", c'est que le mdp a été changé
  # sinon le mdp est portnairAdmin et il faut le modifier
  if ! (echo "$TEST_MDP_PORTAINER" | grep "Invalid credentials" &>/dev/null); then
    # Récupération du jeton pour mettre à jour du mot de passe
    P_TOKEN=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"$P_USER\",\"password\":\"$P_PASS_DEFAUT\"}" "$P_URL/api/auth")
    T=$(echo "$P_TOKEN" | awk -F '"' '{print $4}')
    # Debug
    #echo -e "Le token est $T"
    MAJ_MDP_PORTAINER=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/users/1/passwd" \
        -X PUT \
        -H "Authorization: Bearer $T" \
        -d "{\"newPassword\":\"$P_PASS\",\"password\":\"$P_PASS_DEFAUT\"}"
    )

    echo -e "\nRetour MAJ_MDP_PORTAINER : $MAJ_MDP_PORTAINER" >>"$LOGFILE"
    # Debug
    #echo -e "\nRetour MAJ_MDP_PORTAINER : $MAJ_MDP_PORTAINER"
  fi

  # Récupération du jeton de connexion à l'API de Portainer avec le nouveau mot de passe

  echo -e "\nTentative de connexion à l'API..." >>"$LOGFILE"
  # Ajout du -k pour que les certificats ne soient pas vérifiés
  P_TOKEN=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"$P_USER\",\"password\":\"$P_PASS\"}" "$P_URL/api/auth")
  T=$(echo "$P_TOKEN" | awk -F '"' '{print $4}')
  # Debug
  #echo -e "Le nouveau token est $T"
  if [[ "$P_TOKEN" = *"jwt"* ]]; then
    echo -e "Connexion à l'API de Portainer... Succès" >>"$LOGFILE"
    echo -e "\nConnexion à l'API de Portainer... Succès"
    connect_api="true"
  elif echo "$P_TOKEN" | grep "Invalid credentials"; then
    echo -e "Connexion à l'API de Portainer... ${COLSTOP}Échec. ${COLCMD}Le mot de passe $MDP est différent de celui de Portainer."
    echo -e "\nImpossible d'accéder à l'API de Portainer. Le mot de passe est différent de celui de Portainer." >>"$LOGFILE"
    connect_api="false"
  else
    echo -e "Connexion à l'API de Portainer... ${COLSTOP}Échec. ${COLCMD}Veuillez contacter votre administrateur."
    echo -e "\nImpossible d'accéder à l'API de Portainer. Veuillez contacter votre administrateur." >>"$LOGFILE"
    connect_api="false"
  fi

}

RECUP_VARIABLES_ECB() {

  NGINX_HOST="$ADRESSE_IP_PRIVEE"
  if [ -n "$CHEMIN" ]; then
    CHEMIN_ABSOLU="/$CHEMIN"
    if [ -n "$DOMAINE" ]; then
      FQDN="$DOMAINE/$CHEMIN"
    else
      FQDN="$ADRESSE_IP_PRIVEE/$CHEMIN"
    fi
  else
    CHEMIN_ABSOLU="$CHEMIN"
    if [ -n "$DOMAINE" ]; then
      FQDN="$DOMAINE:$PORT_RP"
      NGINX_HOST="$DOMAINE"
    else
      FQDN="$ADRESSE_IP_PRIVEE:$PORT_RP"
    fi
  fi

  export FQDN NGINX_HOST CHEMIN_ABSOLU

}

CONF_ECB() {

  RECUP_VARIABLES_ECB

  if docker ps -a | grep reseaucerta/e-combox >/dev/null; then
    {
      docker rm -f e-combox
      docker volume rm ecombox_data
      docker volume rm ecombox_config
      docker volume rm ecombox_conf_nginx
    } >>"$LOGFILE" 2>&1

    if docker ps -a | grep reseaucerta/e-combox:"$VERSION_APPLI" >/dev/null; then
      echo -e "e-comBox existe et va être remplacée.\n" >>"$LOGFILE"
      #docker image rm -f reseaucerta/e-combox:"$VERSION_APPLI" >>"$LOGFILE" 2>&1
    fi
  fi

  # Récupération d'une éventuelle nouvelle version d'e-comBox
  echo -e "Téléchargement de l'e-comBox... En cours"
  docker pull reseaucerta/e-combox:"$VERSION_APPLI" >>"$LOGFILE" 2>&1
  if [ $? -eq 0 ]; then
    echo -e "Téléchargement de l'e-comBox... Fait"
    echo -e "L'e-comBox a été téléchargé.\n" >>"$LOGFILE"
  else
    echo -e "$CLOSTOP"
    echo -e "Téléchargement de l'e-comBox... Échec\n"
    echo -e "$COLCMD"
    echo -e "Consultez les log \"$LOGFILE\".\n"
    echo -e "L'e-comBox' n'a pas pu être téléchargé." >>"$LOGFILE"
    exit
  fi

  # Lancement de l'e-comBox
  docker run -dit --name e-combox -v ecombox_conf_nginx:/etc/nginx/conf.d -v ecombox_data:/usr/share/nginx/html/ -v ecombox_config:/etc/ecombox-conf -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 -e VIRTUAL_HOST=app --restart always -p 443 --network bridge_e-combox reseaucerta/e-combox:"$VERSION_APPLI" >>"$LOGFILE" 2>&1

  if [ $? = 0 ]; then
    echo -e "Lancement de l'e-comBox... Succès"
    echo -e "L'e-comBox' a été lancé.\n" >>"$LOGFILE"
  else
    echo -e "$COLSTOP"
    echo -e "Lancement de l'e-comBox... Échec"
    echo -e "$COLCMD"
    echo -e "Consultez les log \"$LOGFILE\".\n"
    echo -e "L'e-comBox' n'a pas pu être lancé." >>"$LOGFILE"
    exit
  fi

  # Dans tous les cas, il faut passer par le reverse proxy de l'e-comBox, il faut donc modifier le fichier index.html et d'autres pour y introduire le chemin d'accès
  # La variable $CHEMIN_ABSOLU est vide si aucun chemin n'a été défini sinon elle contient le chemin avec un / devant

  sed -i "s|<base href=\"/\">|<base href=\"$CHEMIN_ABSOLU/app/\">|g" /var/lib/docker/volumes/ecombox_data/_data/index.html
  #sed -i "s|script src=\"|script src=\"$CHEMIN_ABSOLU/app/|g" /var/lib/docker/volumes/ecombox_data/_data/index.html

  # Configuration de l'API
  URL_EXTRACTION=$(grep -io '("basePath").*api' /var/lib/docker/volumes/ecombox_data/_data/main.*.js | cut -d '"' -f4)
  ANCIENNE_URL_API=$(expr "$URL_EXTRACTION" : '.*\(https.*api\)')

  if [ "$URL_EXTRACTION" ]; then
    # Mise en place de la nouvelle URL de l'API
    NOUVELLE_URL_API="https://$FQDN/portainer/api"
    if [ "$ANCIENNE_URL_API" != "$NOUVELLE_URL_API" ]; then
      for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js; do
        sed -i -e 's#'"$ANCIENNE_URL_API"'#'"$NOUVELLE_URL_API"'#g' "$fichier"
      done
      echo -e "Mise en place de l'URL d'accès à l'API ${NOUVELLE_URL_API}... Fait"
      echo -e "L'URL $ANCIENNE_URL_API est remplacée par $NOUVELLE_URL_API." >>"$LOGFILE"
    else
      echo -e "Aucun changement à opérer au niveau de l'URL d'accès à l'API $ANCIENNE_URL_API."
      echo -e "Aucun changement à opérer au niveau de l'URL $ANCIENNE_URL_API." >>"$LOGFILE"
    fi
  else
    echo -e "$COLSTOP"
    echo -e "Mise en place de l'URL d'accès à l'API $NOUVELLE_URL_API\c... Échec"
    echo -e "$COLCMD"
    echo -e "Consultez les log \"$LOGFILE\".\n"
    echo -e "L'ancienne URL n'a pas été déterminée. Veuillez contacter le support." >>"$LOGFILE"
    exit
  fi
}

STOP_STACKS() {
  # Arrête tous les stacks démarrés

  # Connexion à l'API de Portainer
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi

  # Récupération des stacks
  echo -e "\nRécupération des stacks..." >>"$LOGFILE"
  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  # Stockage des stacks dans un fichier json
  echo "$STACKS" >"$DOSSIER_INSTALL"/stacks.json

  # Récupération du numéro, du nom et du statut de chaque stack
  if [ -n "$STACKS" ]; then
    cat $DOSSIER_INSTALL/stacks.json | jq -c '.[] |{Id,Name,Status}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      status_stack=$(echo "$line" | jq -r '.Status')

      # Arrêt du stack si celui-ci n'est pas déjà arrếté
      if [ "$status_stack" = "1" ]; then
        echo -e "Arrêt du stack $nom_stack..."
        echo -e "\nArrêt du stack $nom_stack..." >>"$LOGFILE"
        STOP=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/stop?endpointId=1" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "\nRetour de l'arrêt : $STOP" >>"$LOGFILE"
      else
        echo -e "Stack $nom_stack déjà arrêté..." >>"$LOGFILE"
      fi
    done
  fi
}

DELETE_STACK() {
  #Supprime un stack dont le nom de stack est passé en paramètre
  NOM_STACK=$1

  # Connexion à l'API de Portainer
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi

  # Récupération des stacks
  echo -e "\nRécupération des stacks..." >>"$LOGFILE"
  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  # Stockage des stacks dans un fichier json
  echo "$STACKS" >"$DOSSIER_INSTALL"/stacks.json

  # Récupération du numéro et du nom de chaque stack
  if [ -n "$STACKS" ]; then
    cat $DOSSIER_INSTALL/stacks.json | jq -c '.[] |{Id,Name}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')

      # Suppression du stack
      if [ "$nom_stack" = "$NOM_STACK" ]; then
        echo -e "Suppression du stack $nom_stack..."
        echo -e "\nSuppression du stack $nom_stack..." >>"$LOGFILE"
        DELETE=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack?endpointId=1" \
            -X DELETE \
            -H "Authorization: Bearer $T"
        )
        echo -e "\nRetour de l'arrêt : $DELETE" >>"$LOGFILE"
      fi
    done
  fi
}

START_STACKS() {
  # Démarre tous les stacks arrếtés

  # Connexion à l'API de Portainer
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi

  # Récupération des stacks
  echo "Récupération des stacks..." >>"$LOGFILE"
  echo ""
  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  echo "$STACKS" >"$DOSSIER_INSTALL"/stacks.json

  if [ -n "$STACKS" ]; then
    cat $DOSSIER_INSTALL/stacks.json | jq -c '.[] |{Id,Name,Status}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      status_stack=$(echo "$line" | jq -r '.Status')

      # Démarrage du stack si celui-ci n'est pas déjà démarré
      if [ "$status_stack" = "0" ]; then
        echo "Démarrage du stack $nom_stack..."
        echo "Démarrage du stack $nom_stack..." >>"$LOGFILE"
        START=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/start?endpointId=1" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "$COLINFO"
        echo -e "\nRetour du démarrage : $START" >>"$LOGFILE"
      else
        echo "Le stack $nom_stack est déjà démarré." >>"$LOGFILE"
      fi
    done
  fi

}

UPDATE_STACKS() {
  if [ ! -d "$DOSSIER_MIGRATION" ]; then
    mkdir "$DOSSIER_MIGRATION"
  fi

  DATE=$(date '+%Y-%m-%d-%H%M')

  echo -e "$COLPARTIE"
  echo -e "la migration des sites peut prendre du temps..."
  echo -e "Migration des sites le $DATE" >"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
  echo -e "$COLCMD"

  # Récupération des docker-compose
  cd "$DOSSIER_MIGRATION" || exit

  if [ -d "$DOSSIER_MIGRATION"/e-comBox_docker-compose/ ]; then
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/
    git clone -b migration-v4 https://"$FORGE"/e-combox/e-comBox_docker-compose.git >>"$LOGFILE" 2>&1
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/.git
  else
    git clone -b migration-v4 https://"$FORGE"/e-combox/e-comBox_docker-compose.git >>"$LOGFILE" 2>&1
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/.git
  fi

  # Connexion à l'API de Portainer
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi

  INFO=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/endpoints/1/docker/info")
  CID=$(echo "$INFO" | awk -F '"Cluster":{"ID":"' '{print $2}' | awk -F '"' '{print $1}')

  {
    echo -e "Cluster ID: $CID"

    # Récupération des stacks
    echo -e "Récupération des stacks...\n"
  } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  echo "$STACKS" >"$DOSSIER_MIGRATION"/stacks_"$DATE".json

  if [ -n "$STACKS" ]; then
    # shellcheck disable=SC2002
    cat "$DOSSIER_MIGRATION"/stacks_"$DATE".json | jq -c '.[] |{Id,Name,EntryPoint,Env}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      dc_yml=$(echo "$line" | jq -r '.EntryPoint')
      env_json=$(echo "$line" | jq -jc '.Env')

      # Modification du nom des docker-compose "custom" qui intégre la référence à une image comportant les nouveaux scripts
      dc_yml=$(echo "$dc_yml" | sed -e 's/custom/neutre/')

      # Modification du nom des docker-compose "kanboard" pour le remplacer par kanboard-vierge (nom du DC a changé entre la v3 et la v4)
      dc_yml=$(echo "$dc_yml" | sed -e 's/docker-compose-kanboard.yml/docker-compose-kanboard-vierge.yml/')

      # Modification du nom des docker-compose "Mautic" pour le remplacer par mautic-v3
      dc_yml=$(echo "$dc_yml" | sed -e 's/docker-compose-mautic.yml/docker-compose-mautic-v3.yml/')

      # Modification du port du registry dans la variable env_json
      env_json=$(echo "$env_json" | jq --arg REGISTRY_PORT "$PORT_REGISTRY" '(.[] | select(.name == "REGISTRY_PORT")).value |= $REGISTRY_PORT')

      {
        echo -e "\nNom du stack traité : $nom_stack"
        echo -e "\nLa variable dc_yml est : $dc_yml"
        echo -e "\nLa variable d'env est $env_json."
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      # Mise à jour du stack
      echo -e "Mise à jour du stack $nom_stack... En cours"
      TARGET_YML="$DOSSIER_MIGRATION/e-comBox_docker-compose/$dc_yml"

      dcompose=$(cat "$TARGET_YML")
      dcompose="${dcompose//$'\r'/''}"
      dcompose="${dcompose//$'"'/'\"'}"

      {
        echo -e "/-----Lecture du fichier YML--------"
        echo -e "$dcompose"
        echo -e "\---------------------"
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      dcompose="${dcompose//$'\n'/'\n'}"
      data_prefix="{\"Id\":\"$num_stack\",\"StackFileContent\":\""
      data_suffix="\",\"Env\":$env_json,\"Prune\":$P_PRUNE}"

      {
        echo "/~~~~Conversion du JSON~~~~~~"
        echo "$data_prefix$dcompose$data_suffix"
        echo "\~~~~~~~~~~~~~~~~~~~~~~~~"
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      echo "$data_prefix$dcompose$data_suffix" >json"$num_stack".tmp

      echo "Mise à jour du stack $nom_stack..." >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      UPDATE=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/stacks/$num_stack?endpointId=1" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -H "Content-Type: application/json;charset=UTF-8" \
          -H 'Cache-Control: no-cache' \
          --data-binary "@json$num_stack.tmp"
      )
      #rm json.tmp

      echo -e "\nRetour de la mise à jour : $UPDATE" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      echo -e "Mise à jour du stack $nom_stack... Fait"

      # Ajout (ou modification) de la restriction (le stack doit appartenir au groupe "Profs")
      RESTRICTION_STACK=$(curl --noproxy "*" -s -k \
        "$P_URL/api/stacks/$num_stack" \
        -X GET \
        -H "Authorization: Bearer $T" | grep ResourceControl)

      if [ -z "$RESTRICTION_STACK" ]; then
        AJOUT_RESTRICTION_STACK=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls" \
            -X POST \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6,\"resourceId\":\"1_$nom_stack\"}"
        )

        echo -e "\nRetour AJOUT_RESTRICTION_STACK : $AJOUT_RESTRICTION_STACK" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      else
        # Récupération de l'ID de restriction
        ID_RESTRICTION_STACK=$(echo "$RESTRICTION_STACK" | jq -r '.ResourceControl.Id')

        # Mise à jour de la restriction
        MAJ_RESTRICTION_STACK=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls/$ID_RESTRICTION_STACK" \
            -X PUT \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6}"
        )
        echo -e "\nRetour MAJ_RESTRICTION_STACK : $MAJ_RESTRICTION_STACK" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      fi
      echo -e "Transfert de propriété du stack $nom_stack au groupe \"Profs\"... Fait"

      # Arrêt du stack sauf le stack FSserver
      if [ ! "$num_stack" = "$ID_STACK_FSSERVER" ]; then
        echo -e "Arrêt du stack $nom_stack... En cours"
        STOP=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/stop" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "Arrêt du stack $nom_stack..." >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
        echo -e "\nRetour de l'arrêt : $STOP" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
        echo -e "Arrêt du stack $nom_stack... Fait"
      fi
    done
    echo -e "$COLINFO"
    echo -e "Les stacks appartiennent maintenant au groupe \"Profs\"."
    echo -e "Ainsi, tous les utilisateurs appartenant à ce groupe pourront agir sur les sites."
    echo -e "L'affectation des sites à un utilisateur en particulier se réalise sur Portainer."
    echo -e "$COLCMD"
  fi

}

GESTION_PROFS() {
  # Connexion à l'API de Portainer
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi

  # Création de l'équipe "Profs"
  GROUPE_PROFS=$(
    curl --noproxy "*" -s -k \
      "$P_URL/api/teams" \
      -X POST \
      -H "Authorization: Bearer $T" \
      -d "{\"name\":\"profs\"}"
  )

  if (echo "$GROUPE_PROFS" | grep "Team already exists" &>/dev/null); then
    echo -e "Équipe \"Profs\"... existe déjà"
    echo -e "L'équipe \"Profs\" existe déjà." >>"$LOGFILE"
    # Récupération de l'ID de l'équipe "Profs"
    GROUPE_PROFS=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/teams" \
        -X GET \
        -H "Authorization: Bearer $T" | grep "profs" | tr -d "[]"
    )
    ID_GROUPE_PROFS=$(echo "$GROUPE_PROFS" | jq -r '.Id')
  else
    if [ "$(echo "$GROUPE_PROFS" | jq -r '.Name')" = "profs" ]; then
      echo -e "Création de l'équipe \"Profs\"... Fait"
      echo -e "L'équipe \"Profs\" a été créée" >>"$LOGFILE"
      # Récupération de l'ID Prof
      ID_GROUPE_PROFS=$(echo "$GROUPE_PROFS" | jq -r '.Id')
    else
      echo -e "$COLSTOP"
      echo -e "Création de l'équipe \"Profs\"... Échec"
      echo -e "$GROUPE_PROFS \n L'équipe \"Profs\" n'a pas pu être créée" >>"$LOGFILE"
      echo -e "$COLCMD"
    fi
  fi

  if [ -n "$ID_GROUPE_PROFS" ]; then
    # Accès à l'équipe "profs" à l'environnement "primary" dans tous les cas car on ne sait jamais :)
    AJOUT_PERMISSIONS=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/{1}" \
        -X PUT \
        -H "Authorization: Bearer $T" \
        -d "{\"TeamAccessPolicies\":{\"$ID_GROUPE_PROFS\":{\"RoleId\":0}}}"
    )

    echo -e "\nRetour AJOUT_PERMISSIONS : $AJOUT_PERMISSIONS" >>"$LOGFILE"

    # On fait la suite dans tous les cas car le serveur nginx est forcément nouvellement créé
    # Restriction sur Nginx
    # Récupération de l'ID du container nginx
    CONTENEUR_NGINX=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/1/docker/containers/nginx/json" \
        -X GET \
        -H "Authorization: Bearer $T"
    )
    ID_CONTENEUR_NGINX=$(echo "$CONTENEUR_NGINX" | jq -r '.Id')

    echo -e "\nRetour ID_CONTENEUR_NGINX : $ID_CONTENEUR_NGINX" >>"$LOGFILE"

    # Création d'une nouvelle restriction si aucune restriction n'existe
    RESTRICTION_NGINX=$(curl --noproxy "*" -s -k \
      "$P_URL/api/endpoints/1/docker/containers/nginx/json" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

    if [ -z "$RESTRICTION_NGINX" ]; then
      AJOUT_RESTRICTION_NGINX=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1,\"resourceId\":\"$ID_CONTENEUR_NGINX\"}"
      )

      echo -e "\nRetour AJOUT_RESTRICTION_NGINX : $AJOUT_RESTRICTION_NGINX" >>"$LOGFILE"

    else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_NGINX=$(echo "$RESTRICTION_NGINX" | jq -r '.Portainer.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_NGINX=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls/$ID_RESTRICTION_NGINX" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1}"
      )
      echo -e "\nRetour MAJ_RESTRICTION_NGINX : $MAJ_RESTRICTION_NGINX" >>"$LOGFILE"

    fi

    # Restriction sur le Registry
    # Récupération de l'ID du container registry
    CONTENEUR_REGISTRY=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/1/docker/containers/e-combox_registry/json" \
        -X GET \
        -H "Authorization: Bearer $T"
    )
    ID_CONTENEUR_REGISTRY=$(echo "$CONTENEUR_REGISTRY" | jq -r '.Id')

    # Création d'une nouvelle restriction si aucune restriction n'existe
    RESTRICTION_REGISTRY=$(curl --noproxy "*" -s -k \
      "$P_URL/api/endpoints/1/docker/containers/e-combox_registry/json" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

    if [ -z "$RESTRICTION_REGISTRY" ]; then
      AJOUT_RESTRICTION_REGISTRY=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1,\"resourceId\":\"$ID_CONTENEUR_REGISTRY\"}"
      )

      echo -e "\nRetour AJOUT_RESTRICTION_REGISTRY : $AJOUT_RESTRICTION_REGISTRY" >>"$LOGFILE"

    else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_REGISTRY=$(echo "$RESTRICTION_REGISTRY" | jq -r '.Portainer.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_REGISTRY=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls/$ID_RESTRICTION_REGISTRY" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1}"
      )
      echo -e "\nRetour MAJ_RESTRICTION_REGISTRY : $MAJ_RESTRICTION_REGISTRY" >>"$LOGFILE"

    fi

    echo -e "\nRetour ID_RESTRICTION_REGISTRY: $ID_RESTRICTION_REGISTRY" >>"$LOGFILE"
  fi

  echo -e "Gestion des permissions... Fait"
  echo -e "Ajout des permissions" >>"$LOGFILE"
}

STOP_SERVICES() {

  # Arrêt des sites
  echo -e "$COLCMD"
  echo -e "Arrêt des sites encore actifs... En cours"
  STOP_STACKS

  # Arrêt de tous les services
  echo -e "Arrêt des services... En cours"

  # Reverse proxy
  cd "$DOSSIER_RP" || exit
  docker compose down

  # Registry
  docker stop e-combox_registry

  # Serveur git
  docker stop e-combox_gitserver

  # Portainer
  docker stop portainer-app
  #docker volume rm e-combox_portainer_portainer-data

  # E-combox
  docker stop e-combox
}

START_SERVICES() {

  # Redémarrage des services
  echo -e "\nRedémarrage des services... En cours"
  # Reverse proxy
  cd "$DOSSIER_RP" || exit
  docker compose up -d

  # Registry
  docker start e-combox_registry

  # Serveur git local
  docker start e-combox_gitserver

  # Portainer
  docker start portainer-app
  # Attente pour que Portainer soit complètement opérationnel
  sleep 5

  # E-combox
  docker start e-combox

  # FSserver
  CONNECTE_API
  if [ "$connect_api" = "false" ]; then
    exit
  fi
  STACK_FSSERVER=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks" | jq -c '.[] | select(.Name == "fsserver")')
  ID_STACK_FSSERVER=$(echo "$STACK_FSSERVER" | jq -r '.Id')
  if [ -n "$STACK_FSSERVER" ]; then
    echo -e "Démarrage du stack \"FSserver\"... En cours"
    echo -e "\nDémarrage du stack \"FSserver\"..." >>"$LOGFILE"
    START_STACK_FSSERVER=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/stacks/$ID_STACK_FSSERVER/start" \
        -X POST \
        -H "Authorization: Bearer $T"
    )
    echo -e "\nRetour du démarrage du stack FSserver: $START_STACK_FSSERVER" >>"$LOGFILE"
    echo -e "Démarrage du stack \"FSserver\"... Fait"
  else
    echo -e "Le stack FSserver n'existe pas" >>"$LOGFILE"
  fi
}

SAUV_SITES() {
  ARCHIVE=$1

  DATE=$(date '+%Y-%m-%d-%H%M')

  if [ -e "$DOSSIER_INSTALL/version3" ]; then
    NOM_ARCHIVE=sitesV3.tar.gz
  else
    # Choix du nom de l'archive si non passé en option
    if [ "$ARCHIVE" = "false" ]; then
      NOM_ARCHIVE_PROPOSEE=sitesV4_$DATE.tar.gz
      echo -e "\nValidez le nom de l'archive de sauvegarde proposée, sinon saisissez un autre nom (l'extension doit obligatoirement être en \".tar.gz\") : ${COLCHOIX}$NOM_ARCHIVE_PROPOSEE"
      read -r NOM_ARCHIVE_SAISIE
      if [ -z "$NOM_ARCHIVE_SAISIE" ]; then
        NOM_ARCHIVE="$NOM_ARCHIVE_PROPOSEE"
      else
        # Test si le nom de l'archive se termine par .tar.gz
        while true; do
          # Vérification si le nom d'archive est valide
          if [[ ! "${NOM_ARCHIVE_SAISIE%.tar.gz}" != "$NOM_ARCHIVE_SAISIE" ]]; then
            echo "Le nom d'archive n'est pas valide. Il doit se terminer par '.tar.gz'. Veuillez réessayer."
            # Lecture du nom d'archive saisi par l'utilisateur
            read -r NOM_ARCHIVE_SAISIE
          fi
        done
        NOM_ARCHIVE="$NOM_ARCHIVE_SAISIE"
      fi
    else
      NOM_ARCHIVE=$(basename "$ARCHIVE")
    fi
  fi

  echo -e "Nom de l'archive... $NOM_ARCHIVE"

  # Choix du dossier de sauvegarde si non passé en option

  if [ "$ARCHIVE" = "false" ]; then

    echo -e "$COLTXT"
    echo -e "L'archive $NOM_ARCHIVE sera stockée par défaut dans le dossier $DOSSIER_INSTALL/sauv."
    echo -e "Voulez-vous choisir un autre dossier ? (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE_DOSSIER
    if [ -z "$REPONSE_DOSSIER" ] || [ "$REPONSE_DOSSIER" = "n" ] || [ "$REPONSE_DOSSIER" = "N" ]; then
      REPONSE_DOSSIER="n"
      DOSSIER_ARCHIVE=$DOSSIER_INSTALL/sauv
      if [ ! -d "/$DOSSIER_INSTALL/sauv" ]; then
        mkdir $DOSSIER_INSTALL/sauv
      fi
    fi
    if [ "$REPONSE_DOSSIER" != "n" ] && [ "$REPONSE_DOSSIER" != "n" ]; then
      # Parcours des dossiers accessibles
      if (! dialog 2>/dev/null); then
        apt update
        apt install dialog
      fi
      DOSSIER_ARCHIVE=$(dialog --stdout --title "Choisissez un dossier. Le chemin vers le dossier voulu peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect / 14 48)
      # Validation du dossier en gérant un éventuel "annuler"
      if [ -z "$DOSSIER_ARCHIVE" ]; then
        echo -e "\nVous n'avez pas choisi de dossier de sauvegarde. Veuillez relancez le script pour sélectionner un nouveau dossier de sauvegarde."
        sleep 2
        exit
      else
        echo -e "\nVous avez choisi le dossier ${DOSSIER_ARCHIVE}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
        read -r REPONSE_DEST
        if [ -z "$REPONSE_DEST" ]; then
          REPONSE_DEST="o"
        fi
        if [ "$REPONSE_DEST" != "o" ] && [ "$REPONSE_DEST" != "0" ]; then
          echo -e "\nVeuillez relancez le script pour sélectionner un nouveau dossier de sauvegarde."
          sleep 2
          exit
        fi
      fi
    fi
    ARCHIVE="$DOSSIER_ARCHIVE"/"$NOM_ARCHIVE"
  else
    DOSSIER_ARCHIVE=$(dirname "$ARCHIVE")
    echo -e "Dossier de sauvegarde... $DOSSIER_ARCHIVE"
  fi

  echo -e "Chemin complet vers l'archive... $ARCHIVE"

  # Sauvegarde des sites

  # Reconstitution du nom du dossier dans lequel sera stocké les fichiers JSON relatifs aux infos de chaque site
  DOSSIER_ARCHIVE_SITE="${ARCHIVE%.tar.gz}"

  # Création du dossier et du fichier de log
  if [ ! -d "$DOSSIER_ARCHIVE"/log ]; then
    mkdir "$DOSSIER_ARCHIVE"/log
  fi
  LOG_ARCHIVE="$DOSSIER_ARCHIVE"/log
  echo -e "Sauvegarde le $DATE" >"$LOG_ARCHIVE"/rapport_sauvegarde_"$DATE".log

  if [ ! -d "$DOSSIER_ARCHIVE_SITE" ]; then
    mkdir "$DOSSIER_ARCHIVE_SITE"
  fi

  # Connexion à l'API de Portainer
  CONNECTE_API

  # Récupération des informations des stacks
  STACKS=$(curl --noproxy "*" -s -k "$P_URL/api/stacks" -X GET -H "Authorization: Bearer $T")
  echo "$STACKS" >"$DOSSIER_ARCHIVE_SITE"/stacks.json

  if [ -n "$STACKS" ]; then
    cat <"$DOSSIER_ARCHIVE_SITE"/stacks.json | jq -c '.[] |{Id,Name,EntryPoint,Env}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      dc_ymL=$(echo "$line" | jq -r '.EntryPoint')
      env_json=$(echo "$line" | jq -jc '.Env')

      # Fichier JSON nécessaire au déploiement du stack
      data_prefix="{\"name\":\"$nom_stack\",\"composeFile\":\"$dc_ymL\",\"Env\":$env_json,"
      data_suffix="\"repositoryAuthentication\": false, \"repositoryReferenceName\": \"refs/heads/dev\", \"repositoryURL\": \"$DEPOT_GITLAB_DC\"}"
      echo "$data_prefix$data_suffix" >"$DOSSIER_ARCHIVE_SITE"/"$nom_stack".json
    done
    echo -e "$COLCMD"
  fi

  ### Archive de l'ensemble des volumes
  # Arrêt des services
  STOP_SERVICES

  # Sauvegarde des sites
  echo -e "\nSauvegarde des sites... En cours, merci de patientez."

  cd /var/lib/docker/ || exit
  #tar -czf "$DOSSIER_ARCHIVE/$ARCHIVE" --preserve-permissions --same-owner volumes >>/"$DOSSIER_ARCHIVE"/sauv.log 2>&1
  tar -czf "$ARCHIVE" --preserve-permissions --same-owner volumes >>"$LOG_ARCHIVE"/rapport_sauvegarde_"$DATE".log 2>&1

  echo -e "Sauvegarde des sites... Terminée."

  # redémarrage des services
  START_SERVICES
}

VALIDE_ARCHIVE() {
  # Validation de l'archive en gérant un éventuel "annuler"
  if [ -z "$ARCHIVE" ]; then
    echo -e "\nVous n'avez pas choisi d'archive à restaurer. Veuillez relancez le script pour sélectionner une archive à restaurer."
    echo -e "$COLCMD"
    sleep 2
    exit
  elif [[ ! "${ARCHIVE%.tar.gz}" != "$ARCHIVE" ]]; then
    echo -e "\nLe nom de l'archive ${ARCHIVE} n'est pas valide. Il doit se terminer par '.tar.gz'.Veuillez relancez le script pour sélectionner une archive à restaurer."
    echo -e "$COLCMD"
    sleep 2
    exit
  else
    echo -e "\nVous avez choisi l'archive ${ARCHIVE}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE_VALIDE_ARCHIVE
    if [ -z "$REPONSE_VALIDE_ARCHIVE" ]; then
      REPONSE_VALIDE_ARCHIVE="o"
      echo -e "$COLCMD"
    fi
    if [ ! "$REPONSE_VALIDE_ARCHIVE" = "o" ] && [ ! "$REPONSE_VALIDE_ARCHIVE" = "O" ]; then
      echo -e "\nVeuillez relancez le script pour sélectionner une archive à restaurer."
      echo -e "$COLCMD"
      sleep 2
      exit
    fi
  fi
}

RESTAURE_SITES() {
  ARCHIVE=$1
  NOM_SITE=$2

  DATE=$(date '+%Y-%m-%d-%H%M')

  # Restauration des sites

  if [ "$NOM_SITE" = "false" ]; then
    echo -e "$COLINFO"
    echo -e "Vous vous apprếtez à restaurer les sites. Attention, la procédure peut être un peu longue."
    echo -e "$COLCMD"
  else
    echo -e "$COLINFO"
    echo -e "Vous vous apprếtez à restaurer le site $NOM_SITE. Attention, la procédure peut être un peu longue."
    echo -e "$COLCMD"
  fi

  # Choix de l'archive à utiliser

  if [ "$ARCHIVE" = "false" ]; then
    echo -e "$COLTXT"
    echo -e "L'archive est stockée par défaut dans le dossier $DOSSIER_INSTALL/sauv."
    echo -e "Voulez-vous choisir un autre dossier ? (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE_DOSSIER
    if [ -z "$REPONSE_DOSSIER" ] || [ "$REPONSE_DOSSIER" = "n" ] || [ "$REPONSE_DOSSIER" = "N" ]; then
      REPONSE_DOSSIER="n"
      DOSSIER_SOURCE=$DOSSIER_INSTALL/sauv

      if (! dialog 2>/dev/null); then
        apt update
        apt install dialog
      fi
      ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le nom de l'archive peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect $DOSSIER_SOURCE/ 14 48)
      VALIDE_ARCHIVE
      echo -e "Archive à restaurer... $ARCHIVE"
    fi
    if [ "$REPONSE_DOSSIER" != "n" ] && [ "$REPONSE_DOSSIER" != "n" ]; then
      # Parcours des dossiers accessibles
      ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le chemin vers le dossier voulu peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect / 14 48)
      VALIDE_ARCHIVE
      echo -e "Archive à restaurer... $ARCHIVE"
    fi
  else
    if [ "$NOM_SITE" = "false" ]; then
      echo -e "Archive à restaurer... $ARCHIVE"
    else
      echo -e "Archive à partir de laquelle sera restaurée le site $NOM_SITE... $ARCHIVE"
    fi
  fi

  # Reconstitution des noms des dossiers liés au(x) site(s) à restaurer
  DOSSIER_ARCHIVE=$(dirname "$ARCHIVE")
  DOSSIER_ARCHIVE_SITE="${ARCHIVE%.tar.gz}"

  # Création du fichier de log
  if [ ! -d "$DOSSIER_ARCHIVE"/log ]; then
    mkdir "$DOSSIER_ARCHIVE"/log
  fi
  LOG_ARCHIVE="$DOSSIER_ARCHIVE"/log
  echo -e "Restauration le $DATE" >"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

  # Restauration d'un seul site

  if [ ! "$NOM_SITE" = "false" ]; then

    echo -e "\nRestauration de $NOM_SITE à partir de $ARCHIVE... En cours"
    echo -e "\nRestauration de $NOM_SITE à partir de $ARCHIVE." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

    # Reconstitution des noms et numéros de stacks ainsi que du fichier json liés au site à restaurer
    TYPE_SITE=$(echo "$NOM_SITE" | cut -d '-' -f1)

    # Définition du nom du stack si le site n'est pas issu d'un modele (sinon le nom du stack ne comporte pas de tirets)
    NOM_STACK="$TYPE_SITE"$(echo "$NOM_SITE" | cut -d '-' -f2-)

    test_nom_stack=$(cat <"$DOSSIER_ARCHIVE_SITE"/stacks.json | jq '.[] | select(.Name=='\""$NOM_STACK"\"')' | jq -r '.Id')

    if [ -z "$test_nom_stack" ]; then
      # Le site est probablement issu d'un modèle
      NOM_STACK=$(echo "$NOM_SITE" | sed "s/-//g")
    fi

    # Récupération des éléments du stack sauvegardées au format json

    NUM_STACK=$(cat <"$DOSSIER_ARCHIVE_SITE"/stacks.json | jq '.[] | select(.Name=='\""$NOM_STACK"\"')' | jq -r '.Id')
    USER_ID=$(cat <"$DOSSIER_ARCHIVE_SITE"/stacks.json | jq '.[] | select(.Name=='\""$NOM_STACK"\"')' | jq -r '.ResourceControl.UserAccesses[0].UserId')

    {
      echo -e "Nom du stack : $NOM_STACK"
      echo -e "Numéro du stack : $NUM_STACK"
      echo -e "Propriétaire du stack : $USER_ID"
    } >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

    # Debug
    #echo -e "dossier_archive $DOSSIER_ARCHIVE"
    #echo -e "nom du stack $NOM_STACK"
    #echo -e "dossier_archive_site $DOSSIER_ARCHIVE_SITE"
    #echo -e "numéro du stack $NUM_STACK"

    if [ -n "$NUM_STACK" ]; then
      # Les informations du stack sont asscociées au fichier JSON récupéré lors de la sauvegarde
      JSON_STACK="$NOM_STACK".json
      echo -e "Éléments sur le site $NOM_SITE à restaurer... Trouvés"
    else
      echo -e "Éléments sur le site $NOM_SITE à restaurer... Non trouvés"
      echo -e "Éléments sur le site $NOM_SITE à restaurer... Non trouvés" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
      echo -e "Restauration du site $NOM_SITE... ${COLSTOP}Échec${COLCMD}"
      exit 1
    fi

    CONNECTE_API

    # Restauration à chaud du site
    echo -e "Restauration du site... En cours"

    # Stock des volumes dans un tableau
    mapfile -t volumes < <(docker volume ls -q)

    # Suppression des anciens volumes s'ils existent
    for volume in "${volumes[@]}"; do
      if [[ "$volume" == "$NOM_STACK"_* ]]; then
        echo -e "Suppression du volume... $volume" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
        echo -e "Suppression du volume... $volume"
        rm -rf "$volume" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1
      fi
    done

    # Restauration des volumes (doivent être restaurés avant le déploiement du stack)
    echo -e "Restauration des volumes... En cours"
    cd /var/lib/docker || exit
    tar -xzf "$ARCHIVE" --preserve-permissions --same-owner --wildcards volumes/"$NOM_STACK"_* >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1
    echo -e "Restauration des volumes... Terminé"

    # Stop du stack s'il existe déjà sinon nécessité de le déployer
    NUM_STACK_NEW=$(curl --noproxy "*" -s -k \
      "$P_URL/api/stacks" \
      -X GET \
      -H "Authorization: Bearer $T" | jq '.[] | select(.Name=='\""$NOM_STACK"\"')' | jq -r '.Id')

    if [ -n "$NUM_STACK_NEW" ]; then
      echo "Site $NOM_STACK... Existe"
      echo "Site $NOM_STACK... Existe déjà. Il faut le stopper." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
      echo "Le nouveau numéro de stack est $NUM_STACK_NEW." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

      STOP=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/stacks/$NUM_STACK_NEW/stop" \
          -X POST \
          -H "Authorization: Bearer $T"
      )
      echo -e "\nRetour de l'arrêt : $STOP" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1
      echo -e "\nRestauration du site... Terminée." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
      echo -e "Restauration du site... Terminée"
    else

      # Déploiement du site
      echo -e "Déploiement du site... En cours"
      cd "$DOSSIER_ARCHIVE_SITE" || exit

      RESTAURE_SITE=$(
        curl --noproxy "*" -s -k "$P_URL/api/stacks?endpointId=1&method=repository&type=2" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -H "Content-Type: application/json;charset=UTF-8" \
          -H 'Cache-Control: no-cache' \
          --data-binary "@$JSON_STACK"
      )

      if [ -z "$RESTAURE_SITE" ]; then
        echo -e "\nRestauration du site... Echec." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
        echo -e "\nRestauration du site... Echec."
        exit 1
      else
        echo -e "Retour sur la restauration du site : $RESTAURE_SITE" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

        # Modification de la restriction si le stack doit appartenir à un utilisateur en particulier
        if [ ! "$USER_ID" = "null" ]; then

          # Récupération du nouveau numéro de stack
          NUM_STACK_NEW=$(curl --noproxy "*" -s -k \
            "$P_URL/api/stacks" \
            -X GET \
            -H "Authorization: Bearer $T" | jq '.[] | select(.Name=='\""$NOM_STACK"\"')' | jq -r '.Id')

          echo "Le nouveau numéro de stack est $NUM_STACK_NEW." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

          # Récupération de l'ID de restriction
          ID_RESTRICTION_STACK=$(curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$NUM_STACK_NEW" \
            -X GET \
            -H "Authorization: Bearer $T" | grep ResourceControl | jq -r '.ResourceControl.Id')

          echo "L'ID de restriction est $ID_RESTRICTION_STACK" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log

          MODIF_RESTRICTION_STACK=$(
            curl --noproxy "*" -s -k \
              "$P_URL/api/resource_controls/$ID_RESTRICTION_STACK" \
              -X PUT \
              -H "Authorization: Bearer $T" \
              -d "{\"AdministratorsOnly\":false,\"Public\":false,\"Users\":[$USER_ID],\"Teams\":[]}"
          )

          echo -e "\nRetour MODIF_RESTRICTION_STACK : $MODIF_RESTRICTION_STACK" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
          #echo -e "\nRetour MODIF_RESTRICTION_STACK : $MODIF_RESTRICTION_STACK"

        fi

        echo -e "\nRestauration du site... Terminée." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log
        echo -e "Restauration du site... Terminée."
      fi
    fi
  else
    # Restauration de tous les sites

    # Arrêt des services
    STOP_SERVICES

    # Suppression du volume data de Portainer qui va être restauré
    docker volume rm e-combox_portainer_portainer-data >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1

    echo -e "Restauration des sites à partir de l'archive $ARCHIVE...  En cours" >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1
    echo -e "Restauration des sites à partir de l'archive $ARCHIVE...  En cours"

    # Suppression des volumes
    cd /var/lib/docker/ || exit
    rm -rf volumes
    tar -xzf "$ARCHIVE" --preserve-permissions --same-owner >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1

    # Redémarrage des services
    START_SERVICES

    echo -e "\nRestauration des sites... Terminée." >>"$LOG_ARCHIVE"/rapport_restauration_"$DATE".log 2>&1
    echo -e "\nRestauration des sites... Terminée."
    echo -e "$COLINFO\nAttention, si vous restaurez sur un autre environnement, il est également nécessaire de reconfigurer l'application.$COLCMD"

  fi
}

USE_CERTIFICAT_NGINX() {

  if [[ -n "$CHEMIN_CERT" && -n "$CHEMIN_KEY" ]]; then
    # Si un chemin vers un certificat est fourni
    # Mise en place des certificats dans nginx
    cp "$CHEMIN_CERT" /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt
    cp "$CHEMIN_KEY" /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key

    # Sauvegarde des certificats dans $DOSSIER_CERTS pour une éventuelle réutilisation
    cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt "$DOSSIER_CERTS"/ecombox.crt
    cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key "$DOSSIER_CERTS"/ecombox.key

  else
    # On utilise le dernier certificat stocké
    cp "$DOSSIER_CERTS"/ecombox.crt /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt
    cp "$DOSSIER_CERTS"/ecombox.key /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key
  fi

  # Reload conf nginx
  docker exec nginx nginx -s reload 2>>"$LOGFILE"

  echo -e "Certificat existant pour le reverse-proxy... Installé"
  echo -e "Le certificat existant a été installé." >>"$LOGFILE"

}

# Vérifie si le certificat nginx auto-signé correspond bien aux paramètres de param.conf
VERIF_CERTIFICAT_NGINX() {
  # Recharge du fichier de paramètres
  source "$FICHIER_PARAM"

  if [ -n "$DOMAINE" ]; then
    CN=$DOMAINE
  else
    CN=$ADRESSE_IP_PRIVEE
  fi

  SUBJECT="subject=C=$CODE_PAYS,ST=$NOM_PAYS,L=$NOM_REGION,O=$NOM_ORGANISATION,CN=$CN"
  if [ -e "$DOSSIER_CERTS"/ecombox.crt ]; then
    SUBJECT_CERT=$(openssl x509 -in "$DOSSIER_CERTS"/ecombox.crt -noout -subject | tr -d " ")
  fi
  #echo -e "SUBJECT=$SUBJECT"
  #echo -e "SUBJECT_CERT=$SUBJECT_CERT"
  if [ "$SUBJECT" = "$SUBJECT_CERT" ]; then
    echo -e "Certficat auto-signé pour le reverse-proxy... Valide"
    echo -e "Pas de certificat à créer." >>"$LOGFILE"
    verif_cert_nginx="true"
  else
    echo -e "Il faut créer un certificat auto-signé." >>"$LOGFILE"
    verif_cert_nginx="false"
  fi
}

CREATE_CERTIFICAT_NGINX() {

  # Recharge du fichier de paramètres
  source "$FICHIER_PARAM"

  if [ -n "$DOMAINE" ]; then
    CN=$DOMAINE
  else
    CN=$ADRESSE_IP_PRIVEE
  fi

  VERIF_CERTIFICAT_NGINX

  # Certificat auto-signé ou certificat perso
  # Si aucun chemin concernant les certificats ne sont fournis
  # on crée un certificat auto-signé si celui-ci n'existe pas déjà
  if [[ -z "$CHEMIN_CERT" || -z "$CHEMIN_KEY" ]] && [ "$verif_cert_nginx" = "false" ]; then
    # Création du certificat auto-signé
    openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=$CN" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt -keyout /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key >>"$LOGFILE" 2>&1
    # Sauvegarde des certificats dans $DOSSIER_CERTS pour une éventuelle réutilisation
    cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt "$DOSSIER_CERTS"/ecombox.crt
    cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key "$DOSSIER_CERTS"/ecombox.key
    echo -e "Création du certificat auto-signé... Fait"

    # Reload conf nginx
    docker exec nginx nginx -s reload >>"$LOGFILE" 2>&1

    echo -e "Certificat auto-signé pour le reverse-proxy... Installé"
    echo -e "Le certificat créé a été installé." >>"$LOGFILE"

  else
    USE_CERTIFICAT_NGINX
  fi

}

# Vérifie si les certificats auto-signés du registry correspondent bien aux paramètres de param.conf
VERIF_CERTIFICAT_REGISTRY() {
  # Recharge du fichier de paramètres
  source "$FICHIER_PARAM"

  # Certificat pour récupérer les images
  if [ -n "$CHEMIN" ]; then
    DNS=$DOMAINE
  else
    if [ -n "$DOMAINE" ]; then
      DNS=$DOMAINE
    else
      DNS=$ADRESSE_IP_PRIVEE:$PORT_RP
    fi
  fi
  # Vérification du certificat pour le push
  SUBJECT_PUSH="subject=C=$CODE_PAYS,ST=$NOM_PAYS,L=$NOM_REGION,O=$NOM_ORGANISATION,CN=localhost"
  if [ -e "$DOSSIER_CERTS"/registry/localhost.crt ]; then
    SUBJECT_PUSH_CERT=$(openssl x509 -in "$DOSSIER_CERTS"/registry/localhost.crt -noout -subject | tr -d " ")
  fi

  # Vérification du certificat pour récupérer les images
  SUBJECT_IMAGE="subject=C=$CODE_PAYS,ST=$NOM_PAYS,L=$NOM_REGION,O=$NOM_ORGANISATION,CN=$DNS"
  if [ -e "$DOSSIER_CERTS"/registry/"$DNS".crt ]; then
    SUBJECT_IMAGE_CERT=$(openssl x509 -in "$DOSSIER_CERTS"/registry/"$DNS".crt -noout -subject | tr -d " ")
  fi

  if [ "$SUBJECT_PUSH" = "$SUBJECT_PUSH_CERT" ]; then
    push="true"
  fi
  if [ "$SUBJECT_IMAGE" = "$SUBJECT_IMAGE_CERT" ]; then
    image="true"
  fi

  if [ "$push" ] && [ "$image" ]; then
    echo -e "Certificats pour le registry... Valides"
    verif_cert_registry="true"
  else
    echo -e "Certificats pour le registry... À créer"
    verif_cert_registry="false"
  fi
}

CREATE_CERTIFICAT_REGISTRY() {

  # Recharge du fichier de paramètres
  source "$FICHIER_PARAM"

  VERIF_CERTIFICAT_REGISTRY

  # Création des certificats auto-signé s'ils ne sont pas à jour
  date_auj=$(date +%s)
  # Date d'expiration du certificat pour le registry
  if [ -e "$DOSSIER_CERTS"/registry/localhost.crt ]; then
    date_exp=$(date -d "$(openssl x509 -in "$DOSSIER_CERTS"/registry/"$DNS".crt -noout -enddate | cut -d"=" -f2)" +"%s")
  fi
  if [ "$verif_cert_registry" = "false" ] || [ "$(echo "$date_auj" | tr -d -)" -ge "$(echo "$date_exp" | tr -d -)" ]; then
    # Arrêt du registry
    if docker ps -a | grep e-combox_registry; then
      docker rm -f e-combox_registry 2>>"$LOGFILE"
      echo -e "Le registry existe mais n'a pas le bon certificat. Il sera supprimé puis recréé." >>"$LOGFILE"
    fi

    # Création des deux certificats nécessaires pour le registry
    # Ceux-ci sont différents du certificat pour nginx

    # Certificat pour le push
    openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=localhost" -addext "subjectAltName = DNS:localhost" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -keyout "$DOSSIER_CERTS"/registry/localhost.key -out "$DOSSIER_CERTS"/registry/localhost.crt >>"$LOGFILE" 2>&1
    echo -e "Création du certificat pour le push du Registry... Fait"
    echo -e "Le certificat pour le push du Registry a été créé." >>"$LOGFILE"

    # Certificat pour récupérer les images
    if [ -n "$CHEMIN" ]; then
      DNS=$DOMAINE
    else
      if [ -n "$DOMAINE" ]; then
        DNS=$DOMAINE
      else
        DNS=$ADRESSE_IP_PRIVEE:$PORT_RP
      fi
    fi
    openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=$DNS" -addext "subjectAltName = DNS:$DNS" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -keyout "$DOSSIER_CERTS"/registry/"$DNS".key -out "$DOSSIER_CERTS"/registry/"$DNS".crt >>"$LOGFILE" 2>&1
    echo -e "Création du certificat pour la récupération des images... Fait"
    echo -e "Le certificat pour la récupération des images a été créé." >>"$LOGFILE"

    # Mise en place des certificats client
    mkdir -p /etc/docker/certs.d/localhost:"$PORT_REGISTRY"
    mkdir -p /etc/docker/certs.d/"$DNS"
    cp "$DOSSIER_CERTS"/registry/localhost.crt /etc/docker/certs.d/localhost:"$PORT_REGISTRY"/
    cp "$DOSSIER_CERTS"/registry/"$DNS".crt /etc/docker/certs.d/"$DNS"/
    echo -e "Certificats pour le registry... Installés"
    echo -e "Les certificats pour le Registry ont été installés." >>"$LOGFILE"

  fi
}

CONFIGURE_OAUTH() {

  if [ "$OAUTH_ENABLE" = "true" ] || [ "$OAUTH_ENABLE" = "True" ]; then

    if [[ -n "$SSO_ENABLE" && -n "$CLIENT_ID" && -n "$CLIENT_SECRET" && -n "$AUTHORIZATION_URL" && -n "$ACCESS_TOKEN_URL" && -n "$RESOURCE_URL" && -n "$LOGOUT_URL" && -n "$USER_IDENTIFIER" && -n "$SCOPES" ]]; then

      CONNECTE_API
      if [ "$connect_api" = "false" ]; then
        exit
      fi

      # URL utilisée par le fournisseur OAuth pour rediriger l'utilisateur après une authentification réussie
      # Doit être défini sur l'URL de l'instance Portainer
      if [ -z "$FQDN" ]; then
        FQDN=$(grep FQDN "$DOSSIER_RP"/.env | cut -d'=' -f2)
      fi
      REDIRECT_URL="https://$FQDN/app/ecombox/dashboard"

      # ID de l'équipe "Profs" ("1" Normalement mais on ne sait jamais)
      GROUPE_PROFS=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/teams" \
          -X GET \
          -H "Authorization: Bearer $T" | grep "profs" | tr -d "[]"
      )
      ID_GROUPE_PROFS=$(echo "$GROUPE_PROFS" | jq -r '.Id')

      # Configuration de OAuth

      # Paramétrage de la méthode d'authentification sur OAuth
      P_METHODE_0AUTH="{
       \"AuthenticationMethod\":3
    }"

      FIXE_METHODE_OAUTH=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/settings" \
          -H "Authorization: Bearer $T" -X PUT \
          -H "Content-Type: application/json;charset=UTF-8" \
          -H "Cache-Control: no-cache" \
          -d "$P_METHODE_0AUTH"
      )
      echo -e "\nParamétrage de la méthode d'authentification sur OAuth : $FIXE_METHODE_OAUTH" >>"$LOGFILE" 2>&1

      # Configuration de l'authentification OAuth
      P_SETTINGS="{
        \"OAuthSettings\": {
           \"ClientID\": \"$CLIENT_ID\",
           \"AccessTokenURI\": \"$ACCESS_TOKEN_URL\",
           \"AuthorizationURI\": \"$AUTHORIZATION_URL\",
           \"ResourceURI\": \"$RESOURCE_URL\",
           \"RedirectURI\": \"$REDIRECT_URL\",
           \"UserIdentifier\": \"$USER_IDENTIFIER\",
           \"Scopes\": \"$SCOPES\",
           \"OAuthAutoCreateUsers\": true,
           \"DefaultTeamID\": $ID_GROUPE_PROFS,
           \"SSO\": $SSO_ENABLE,
           \"LogoutURI\": \"$LOGOUT_URL\",
           \"KubeSecretKey\": \"$CLIENT_SECRET\"
        }
    }"

      FIXE_CONFIG_OAUTH=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/settings" \
          -H "Authorization: Bearer $T" -X PUT \
          -H "Content-Type: application/json;charset=UTF-8" \
          -H "Cache-Control: no-cache" \
          -d "$P_SETTINGS"
      )
      echo -e "\nConfiguration de l'authentification à Portainer avec OAuth :\n $FIXE_CONFIG_OAUTH" >>"$LOGFILE" 2>&1

      echo -e "Configuration OAuth... Fait"

      # Ajout du certificat du fournisseur d'identité (sinon Portainer renvoie une erreur)
      if [ -e "$CERT_OAUTH" ]; then
        {
          CERT=$(basename "$CERT_OAUTH")
          docker cp "$CERT_OAUTH" portainer-app:/etc/ssl/certs/
          docker restart portainer-app
        } >>"$LOGFILE" 2>&1
        echo -e "Certificat du fournisseur d'identité... Installé"
        echo -e "Certificat du fournisseur d'identité $CERT... Installé. Le chemin complet est $CERT_OAUTH." >>"$LOGFILE"
      else
        echo -e "Certificat du fournisseur d'identité... Non trouvé"
        echo -e "Certificat du fournisseur d'identité $CERT... Non trouvé. Le chemin complet est $CERT_OAUTH." >>"$LOGFILE"
      fi
    else
      echo -e "${COLSTOP}Paramètres manquants pour la configuration de l'authentification OpenD Connect${COLCMD}"
    fi
  else
    CONNECTE_API
    if [ "$connect_api" = "false" ]; then
      exit
    fi
    P_METHODE_INTERNE="{
       \"AuthenticationMethod\":1
    }"

    # Modification de la méthode d'authentification pour basculer sur l'authentification interne

    FIXE_METHODE_INTERNE=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/settings" \
        -H "Authorization: Bearer $T" -X PUT \
        -H "Content-Type: application/json;charset=UTF-8" \
        -H "Cache-Control: no-cache" \
        -d "$P_METHODE_INTERNE"
    )

    echo -e "\nConfiguration de la méthode d'authentification sur l'authentification interne : $FIXE_METHODE_INTERNE" >>"$LOGFILE" 2>&1
    echo -e "\nConfiguration méthode d'authentification interne... Fait"

  fi
}

CLEAN_VOLUMES() {
  # Suppression des volumes qui ne sont associés à aucun stack

  # Récupération des stacks
  CONNECTE_API
  echo -e "\nAjout des noms de stacks dans un fichier..." >>"$LOGFILE"
  curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks" | jq -c '.[] |{Name}' | jq -r '.Name' >noms_stacks.txt

  # Stock des noms de stack dans un tableau
  mapfile -t stacks <noms_stacks.txt

  # Stock des volumes dans un tableau
  mapfile -t volumes < <(docker volume ls -q)

  # Parcours de la liste des volumes et vérification quant à la correspondance à un stack
  for volume in "${volumes[@]}"; do
    #echo "$volume"
    matched=false
    for stack in "${stacks[@]}"; do
      #echo "$stack"
      # Si le nom du stack est présent dans le nom du volume
      if [[ "$volume" == *"$stack"* ]]; then
        matched=true
        break
      fi
    done

    if [ "$matched" = false ]; then
      if ! [[ "$volume" == ecombox* || "$volume" == e-combox* || $volume == portainer* || $volume == registry* || $volume = metadata.db ]]; then
        echo "Suppression du volume non utilisé... $volume"
        echo "Suppression du volume non utilisé : $volume"
        docker volume rm "$volume" >/dev/null >>"$LOGFILE" 2>&1
      fi
    fi
  done

}

ALIMENTATION_ML() {
  # Alimentation d'un élément des mentions légales ou de la politique de confidentialité à partir de param.conf
  # Uniquement si cet élément n'existe pas déjà

  # Nom du paramètre utilisé dans param.conf
  VAR_PARAM=$1
  # Nom du fichier cible dans le volume de FSserver
  FICHIER=$2
  # Volume du conteneur FSserver utilisé pour les mentions légales
  VOLUME_ML="/var/lib/docker/volumes/fsserver_data/_data"
  # ${!VAR_PARAM} est utilisé pour accéder à la valeur de la variable dont le nom est contenu dans VAR_PARAM
  if [ -n "${!VAR_PARAM}" ] && [ ! -e "$VOLUME_ML"/"$FICHIER" ]; then
    echo "${!VAR_PARAM}" >"$VOLUME_ML"/"$FICHIER"
    # shellcheck disable=SC2034
    ALIMENTATION_ML=true
  fi
}

MODIF_ML() {
  # Modification d'un élément des mentions légales ou de la politique de confidentialité
  # Écrase un éventuel élément existant
  # Nom du paramètre utilisé dans param.conf
  VAR_PARAM=$1
  # Nom du fichier cible dans le volume de FSserver
  FICHIER=$2
  # Volume du conteneur FSserver utilisé pour les mentions légales
  VOLUME_ML="/var/lib/docker/volumes/fsserver_data/_data"
  # ${!VAR_PARAM} est utilisé pour accéder à la valeur de la variable dont le nom est contenu dans VAR_PARAM
  if [ -n "${!VAR_PARAM}" ]; then
    echo "${!VAR_PARAM}" >"$VOLUME_ML"/"$FICHIER"
    # shellcheck disable=SC2034
    MODIF_ML=true
  fi
}
