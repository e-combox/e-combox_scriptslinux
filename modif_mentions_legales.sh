#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

# Modification des mentions légales et de la politique de confidentialité à partir de param.conf
# Écrase un éventuel élément existant
MODIF_ML PROPRIETAIRE owner
MODIF_ML ADRESSE_POSTALE address
MODIF_ML IMMATRICULATION registration
MODIF_ML HEBERGEUR host
MODIF_ML RESPONSABLE_TRAITEMENT manager
MODIF_ML ADMIN_SERVICE admin
MODIF_ML DPO dpd

if [ "$MODIF_ML" = true ]; then
    echo -e "Modification des mentions légales... Fait"
    echo -e "Modification des mentions légales... Fait" >>"$LOGFILE"
else
    echo -e "Pas de modification des mentions légales." >>"$LOGFILE"
fi
