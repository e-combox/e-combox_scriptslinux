# Changelog | Liste des changements

C'est la version qui permet de déployer l'e-comBox à grande échelle dans les académies et dans les régions.

## [4.3.0]

**Date de sortie** : mars 2023

### Fonctionnalités

- **Réinitialisation d'un site** : ajout d'un bouton sur la carte d'un site permettant de le restaurer à son état initial.
- **Accès aux sites** : ajout d'un lien pour l'accès direct des sites au front-end.
- **Ajout du lien vers le site recensant les modèles**
- **Alimentation automatique des mentions légales à partir de param.conf** : utile pour ceux qui déploient de nombreuses instances avec des mentions légales identiques.

### Sécurité

- **Mise à jour des images** : réduction des vulnérabilités.

### Divers

- **Reverse-Proxy e-comBox** : configuration de nginx permettant d'accéder à différents services tiers à partir de l'application e-comBox (suppression de l'utilisation de cors-anywhere)
- **Formatage des Docker Compose** : passage des fichiers Docker Compose au dernier format compatible (version v2 de Docker compose).  
- **Optimisations diverses**

## [4.2.2]

**Date de sortie** : septembre 2023

### **Divers**

- **Résolution des bugs**
- **Optimisations diverses**

## [4.2.1]

**Date de sortie** : Juillet 2023

### **Divers**

- **Résolution des bugs**
- **Optimisations diverses**
- **Améliorations liées à la sécurité des images Docker**

## [4.2.0]

**Date de sortie** : juillet 2023

### **Fonctionnalités**

- **Authentification** : module permettant l'authentification des professeurs sur l'interface e-comBox via OAuth.
- **Automatisation de la sauvegarde/restauration**
- **Possibilité de ne restaurer qu'un site**

### **Divers**

- **Portainer** : mise à jour de la version, intégration de traces plus développées, intégration des certificats Let's Encrypt intermédiaires, possibilité de lui ajouter facilement un certificat, ne démarre plus via un docker-compose.
- **Optimisations diverses**
- **Améliorations liées à la sécurité des images Docker**

## [4.1.0] (2023-02-21)

### Installation

- **Installation facilitée** : installation automatisée (sans interaction avec l'administrateur).
- **Déploiement via Ansible** : exemples de playbook permettant un déploiement automatisé de l'application.

### Gestion de l'application

- **Script delete_application.sh** : permet de désinstaller proprement l'application et éventuellement Docker et Docker Compose.
- **Script reinitialise_application.sh** : permet de réinitialiser l'application pour repartir sur un environnement "propre".
- **Script manage_certificats.sh** : permet de créer un certificat Let's Encrypt et de mettre à jour les certificats existants.
- **Script manage_images.sh** : permet de supprimer et de mettre à jour les images.

## [4.0.0] (2022-11-20)

### Installation

- **Reverse Proxy** : possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports (mis à part le port d'accès à l'interface) sur le serveur et à ne dédier qu'une seule adresse IP publique pour l'ensemble des instances.

### Gestion de l'application

- **Portainer** : synchronisation automatique de l'application avec le mot de passe défini par l'administrateur dans Portainer.
- **Script reset_pass_portainer** : permet de réinitialiser le mot de passe de Portainer.

### Sécurité

- **HTTPS** : intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox).

## [3.0.0] (2021-07-06)

### Gestion de l'application

- **Sauvegarde/restauration** globale (en demandant un chemin à l’utilisateur).

## [2.0.0] (2020-12-18)

### Sécurisation des sites et de l'application

- Accès aux sites à partir d’une URL (par exemple : <http://adresseIP:troisièmePort/nom_site>) en masquant la redirection vers les différents ports via un reverse proxy : seuls 3 ports plus les ports pour le SFTP (si ce dernier est utilisé) allant de 2200-2299 doivent être ouverts via un pare-feu.
- Choix possible des ports utilisés par l’application. Les ports 8800 (pour l'accès au sites), 8880 (pour l'accès à l'administration avancée) et 8888 (pour l'accès à l'application) sont proposés par défaut.
- Les mots de passe n'apparaissent plus dans les Docker Compose.
- Accès à distance à l'interface d'e-comBox avec possibilité de créer un nom d'utilisateur et un mot de passe.
- Modification possible du mot de passe du compte "admin" de Portainer (administration avancée).
