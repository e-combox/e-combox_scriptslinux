#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

SHOW_USAGE() {
    echo -e "\nCe script permet de réinitialiser l'application e-comBox."
    echo -e "L'option \"-a\" est obligatoire, elle force la suppression des conteneurs, volumes, réseaux et images."
    echo -e "L'option -f force la suppression même si l'accès à Portainer n'est plus possible"
    echo -e "L'ajout de l'option \"-n\" permet de réinstaller l'application."
    echo -e "Sans l'option \"-n\", l'application n'est pas réinstallée mais est prête à l'être, l'ancien fichier param.conf sera sauvegardé en param.sauv.conf et un fichier param.conf contenant les valeurs pas défaut sera téléchargé."
    echo -e "L'option \"-p\" est obligatoire (dans le cadre d'une réinstallation uniquement) si un mot de passe pour Portainer n'a pas été renseigné dans le param.conf."
    echo -e "Le mot de passe ne peut pas contenir les caractères \" et \$."
    echo -e "Les autres options servent à la réinstallation de l'application pour alimenter le fichier param.conf (voir Usage ci-après)."
    echo -e "Usage: bash $0 -a -f [-n] [-v \"valeur\"] [-i \"valeur\"] [-d \"valeur\"] [-r \"valeur\"] [-c \"valeur\"] [-p \"valeur\"] [s | b] [-h]"
    echo -e "\t-a\t\tSuppression de tous les conteneurs, volumes, réseaux et images."
    echo -e "\t-f\t\tForce la suppression même si l'accès à Portainer n'est plus possible."
    echo -e "\t-n\t\tRéinstallation de l'application."
    echo -e "\t-v\t\tVersion de l'application à installer si vous voulez changer de version [-v \"version\"]."
    echo -e "\t-i\t\tAdresse IP privée ou nom de domaine correspondant à une adresse IP privée  [-i \"@IP_PRIVEE\" | -i \"nom_domaine\"]."
    echo -e "\t-d\t\tAdresse IP publique ou nom de domaine correspondant à une adresse IP publique  [-d \"@IP_PUBLIQUE\" | -d \"nom_domaine\"]. Pour supprimer une valeur existante -d \"\"."
    echo -e "\t-r\t\tPassage par un Reverse-Proxy externe ou non  [-r \"O\" | -r \"N\"]."
    echo -e "\t-c\t\tChemin en cas de Reverse-Proxy externe [-c \"chemin\"]. Pour supprimer un chemin existant -c \"\"."
    echo -e "\t-p\t\tMot de passe de Portainer [-p \"mot_de_passe\"]. Requis dans le cadre d'une réinitialisation. Les caractères suivants \" $ \` \\ & \| ! [space] ne peuvent pas être utilisés en ligne de commande."
    echo -e "\t-s\t\tMet la variable de suppression des images à \"true\" le temps d'exécution du script [-s]."
    echo -e "\t-b\t\tMet la variable de suppression des images à \"false\" le temps d'exécution du script [-n]."
    echo -e "\t-h\t\tDétail des options."
}

# Initialisation des options passées au script.
# Toutes les options sont passées au script start_configuration_application.sh pour la réinstallation.
while getopts "afnv:i:d:r:c:p:sbh" option; do
    # a: suppression des conteneurs, volumes, réseaux et images
    # f: force la suppression de l'application même si Portainer n'est plus accessible
    # n: réinstallation de l'e-comBox
    # v: changement de version
    # i: adresse ip privée
    # d: domaine
    # r: passage par un reverse-proxy
    # c: chemin
    # p: mot de passe de portainer
    # s: met la variable "DEL_IMAGES à "true" le temps de l'exécution du script
    # b: met la variable "DEL_IMAGES à "false" le temps de l'exécution du script

    case $option in
    a) remove="true" ;;
    f) del_stop_stack="true" ;;
    n) reinstall="true" ;;
    v) version_o=$OPTARG ;;
    i)
        adresse_ip_privee_o=$OPTARG
        options="${options} -i $OPTARG"
        ;;
    d)
        chemin_o=$OPTARG
        # Pour effacer éventuellement un chemin existant
        if [ -z "$OPTARG" ] || [ "$OPTARG" = "" ]; then
            OPTARG="NULL"
        fi
        options="${options} -d $OPTARG"
        ;;
    r)
        rp_ext_o=$OPTARG
        options="${options} -r $OPTARG"
        ;;
    c)
        domaine_o=$OPTARG
        # Pour effacer éventuellement un domaine existant
        if [ -z "$OPTARG" ] || [ "$OPTARG" = "" ]; then
            OPTARG="NULL"
        fi
        options="${options} -c $OPTARG"
        ;;
    p)
        mdp_portainer_o=$OPTARG
        options="${options} -p $OPTARG"
        ;;
    s)
        del_images_o="true"
        options="${options} -s"
        ;;
    b)
        del_images_o="false"
        options="${options} -b"
        ;;

    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n${COLSTOP}Relancez le script avec la ou les bonnes options.$COLCMD"
        SHOW_USAGE
        echo -e "\nOption invalide.\n" >>"$LOGFILE"
        exit 1
        ;;
    esac
done

# Vérification de la cohérence des options passées
# Si l'option "-n" n'est pas passée, il n'y a aucune raison de passer les autres options
if [[ -n "$remove" && -z "$reinstall" ]] && [[ -n "$version_o" || -n "$adresse_ip_privee_o" || -n "$chemin_o" || -n "$rp_ext_o" || -n "$domaine_o" || -n "$mdp_portainer_o" || -n "$del_images_o" ]]; then
    echo -e "$COLSTOP"
    echo -e "Si vous voulez réinstaller l'application, ajoutez l'option \"-n\" sinon vous ne devez pas préciser d'options supplémentaires."
    echo -e "$COLCMD"
    SHOW_USAGE
    exit
fi

# Avant de faire quoi que ce soit, on vérifie si, en cas de réinitialisation,
# le mot de passe de Portainer est renseigné et valide
if [[ -n "$remove" && -n "$reinstall" ]]; then
    source "$FICHIER_PARAM"

    if [ -z "$mdp_portainer_o" ] && [ -z "$MDP_PORTAINER" ]; then
        echo -e "$COLSTOP"
        echo -e "Le mot de passe de Portainer a été réinitialisé, il est nécessaire d'en renseigner un, soit en passant l'option \"-p mot_de_passe\" soit en le renseignant dans le fichier \"param.conf\"."
        echo -e "$COLCMD"
        echo -e "Mot de passe non renseigné lors de la réinitialisation" >>"$LOGFILE"
        exit
    fi
    if [ -n "$mdp_portainer_o" ]; then
        # Debug
        #echo "Le mot de passe passé dans reinitialisation_application est $mdp_portainer_o"
        VERIF_MDP_PORTAINER "$mdp_portainer_o" "$reinstall"
        if [ "${mdp_p:?}" = "false" ]; then
            exit
        fi
    fi
fi

if [ -z "$remove" ]; then
    # On doit forcer la suppression en passant l'option -a
    echo -e "$COLSTOP"
    echo -e "L'option \"-a\" est obligatoire."
    echo -e "$COLCMD"
    SHOW_USAGE
    exit 1
else
    # Création ou réinitialisation du fichier de log
    echo -e "\n${COLDEFAUT}Suppression des conteneurs et réinitialisation d'e-comBox le $(date)${COLCMD}"
    echo -e "\nSuppression des conteneurs et réinitialisation d'e-comBox le $(date)\n" >>"$LOGFILE"
    echo -e "...\n"

    # Arrêt des stacks et conteneurs
    if (docker ps -a >>"$LOGFILE" 2>&1); then
        echo -e "Arrêt des sites... En cours"
        if (docker ps | grep "portainer-app" >>"$LOGFILE" 2>&1); then
            if [ ! "$del_stop_stack" = "true" ]; then
                STOP_STACKS
            fi
        fi
        # shellcheck disable=SC2046
        docker stop $(docker ps -aq) >>"$LOGFILE" 2>&1
        # Redémarrage du démon
        systemctl daemon-reload
    fi

    # Suppression des conteneurs, volumes, réseaux images
    echo -e "\nConteneurs, volumes, réseaux et images... En cours"
    docker system prune -a -f --volumes >>"$LOGFILE" 2>&1

    # Suppression des volumes (dans certains cas, ils ne se suppriment pas avec la commande précédente)
    if (docker volume ls --format "{{.Name}}" >>"$LOGFILE" 2>&1); then
        # Debug
        #echo -e "\nSuppression des volumes qui ne se sont pas supprimés avec docker prune."
        echo -e "\nSuppression des volumes qui ne se sont pas supprimés avec docker prune." >>"$LOGFILE"
        #shellcheck disable=SC2046
        docker volume rm $(docker volume ls --format "{{.Name}}") >>"$LOGFILE" 2>&1
    fi

    echo -e "\nConteneurs, volumes, réseaux et images... Supprimés"
    echo -e "\nConteneurs, volumes, réseaux et images... Supprimés" >>"$LOGFILE"

    # Suppression des dossiers et fichiers
    rm -rf "$DOSSIER_RP"

    #rm -rf "$DOSSIER_INSTALL/certs"
    rm -rf "$DOSSIER_INSTALL/secret_key"
    rm -rf "$DOSSIER_INSTALL/.mdp_portainer_chiffre.txt"

    echo -e "Mot de passe de Portainer... Supprimé.${COLCMD}"
    echo -e "Suppression du mot de passe de portainer.\n" >>"$LOGFILE"

    # Pour le cas où le configure_application aurait été supprimé
    if [ ! -e $DOSSIER_INSTALL/configure_application.sh ]; then
        curl -fsSL https://forge.aeif.fr/e-combox/e-combox_scriptslinux/raw/"$BRANCHE"/configure_application.sh -o "$DOSSIER_INSTALL"/configure_application.sh
    fi
fi

# Réinitialisation de l'application

if [ "$reinstall" = "true" ]; then

    # Changement éventuelle de la version de l'application
    if [ -n "$version_o" ] && [ "$version_o" != "$BRANCHE" ]; then
        sed -i "s|^BRANCHE=.*|BRANCHE=$version_o|" "$DOSSIER_INSTALL/configure_application.sh"
        BRANCHE=$version_o
    fi

    # Prise en compte des options pour lancer l'application
    if [ -n "$options" ]; then
        echo -e "\nOptions de la réinstallation d'e-comBox... ${options:1}"
        echo -e "\nOptions de la réinstallation d'e-comBox :${options:1}" >>"$LOGFILE"
    else
        echo -e "Aucune option passée pour la réinstallation." >>"$LOGFILE"
    fi

    # Ne pas mettre ${options:1} entre guillemets de manière à ce que l'argument passé ne soit pas considéré comme une chaîne entière
    # shellcheck disable=SC2086
    bash "$DOSSIER_INSTALL"/configure_application.sh ${options:1}
    # Selon les caractères spéciaux dans le mdp, le script ne s'arrête pas
    exit 0
else
    # On sauvegarde le fichier param.conf en param.sauv.conf
    cp "$FICHIER_PARAM" $DOSSIER_INSTALL/param.sauv.conf
    chmod 600 $DOSSIER_INSTALL/param.sauv.conf

    # On télécharge le param.conf depuis gitlab
    curl -fsSL "$DEPOT_GITLAB_SCRIPTS"/raw/"$BRANCHE"/param.conf -o "$FICHIER_PARAM"
    chmod 600 "$FICHIER_PARAM"
fi
