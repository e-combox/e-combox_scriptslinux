#!/bin/bash
# shellcheck source=/dev/null

# Réinitialisation du mot de passe du compte admin de Portainer

SHOW_USAGE() {
    echo -e "\nCe script permet de réinitialiser le mot de passe de Portainer."
    echo -e "L'option -f doit obligatoirement être passée en ligne de commande (voir Usage ci-après)."
    echo -e "À noter que vous devez ensuite vous connecter sur Portainer pour modifier le mot de passe fourni afin que ce dernier soit compatible (il ne doit pas contenir les caractères spéciaux \" et \`)."
    echo -e "Usage: $0 -f [-h]"
    echo -e "\t-f\t\tRéinitialisation du mot de passe de Portainer"
    echo -e "\t-h\t\tDétail des options"
}

# Initialisation des paramètres passés au script.
while getopts "fh" option; do
    # f: Forcer la réinitialisation du mot de passe

    case $option in
    f)
        docker pull portainer/helper-reset-password
        docker stop portainer-app
        echo -e "$COLINFO"
        docker run --rm -v e-combox_portainer_portainer-data:/data portainer/helper-reset-password
        echo -e "$COLCMD"
        docker start portainer-app
        init="True"
        ;;
    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        exit 1
        ;;
    esac
done

# Vérification des données
if [ -z "$init" ]; then
    SHOW_USAGE
    exit 1
fi
