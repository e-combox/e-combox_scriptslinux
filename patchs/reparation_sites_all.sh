#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Ce script répare tous sites presta et WP qui ont été lancés avec les images de la version 4.3 (mais tagguées en 4.0)
# Et pour lesquels cela a posé un problème

# Appel des fichiers de variables, de fonctions et de param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$DOSSIER_INSTALL"/param.conf

DATE=$(date '+%Y-%m-%d-%H%M')
LOG="$DOSSIER_INSTALL"/rapport_reparation_"$DATE".log

echo -e "Réparation des stacks le $DATE...\n" >"$LOG"

# Connexion à l'API de Portainer
CONNECTE_API
# shellcheck disable=SC2154
if [ "$connect_api" = "false" ]; then
    exit
fi

# Répertoire contenant les fichiers temporaires
DOSSIER_TEMP="$DOSSIER_INSTALL/temp"
if [ ! -d "$DOSSIER_TEMP" ]; then
    mkdir $DOSSIER_TEMP
fi

# Répertoire contenant les DC utilisés par les stacks
DOSSIER_DC="/var/lib/docker/volumes/e-combox_portainer_portainer-data/_data/compose/"

# Récupération des stacks
echo -e "Récupération des stacks...\n" >>"$LOG"
STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")
echo "$STACKS" >"$DOSSIER_TEMP"/stacks_"$DATE".json

# Traitement
# shellcheck disable=SC2002
cat "$DOSSIER_TEMP"/stacks_"$DATE".json | jq -c '.[] |{Id,Name,Status,EntryPoint}' | while read -r line; do
    num_stack=$(echo "$line" | jq -r '.Id')
    nom_stack=$(echo "$line" | jq -r '.Name')
    status_stack=$(echo "$line" | jq -r '.Status')
    dc_yml=$(echo "$line" | jq -r '.EntryPoint')
    # Démarrage du stack si celui-ci n'est pas déjà démarré
    #echo -e " Le status du stack $nom_stack est $status_stack"
    if [ ! "$status_stack" = "1" ]; then
        echo "Démarrage du stack $nom_stack..."
        echo "Démarrage du stack $nom_stack..." >>"$LOG" 2>&1
        START=$(
            curl --noproxy "*" -s -k \
                "$P_URL/api/stacks/$num_stack/start?endpointId=1" \
                -X POST \
                -H "Authorization: Bearer $T"
        )
        echo -e "\nRetour du démarrage : $START" >>"$LOG"

    else
        echo -e "Le stack $nom_stack est déjà démarré."
        echo -e "Le stack $nom_stack est déjà démarré." >>"$LOG"
    fi
done

# On récupère à nouveau les stacks
echo -e "\nRécupération des stacks...\n" >>"$LOG"
STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")
echo "$STACKS" >"$DOSSIER_TEMP"/stacks_"$DATE".json
# shellcheck disable=SC2002
cat "$DOSSIER_TEMP"/stacks_"$DATE".json | jq -c '.[] |{Id,Name,Status,EntryPoint}' | while read -r line; do
    num_stack=$(echo "$line" | jq -r '.Id')
    nom_stack=$(echo "$line" | jq -r '.Name')
    status_stack=$(echo "$line" | jq -r '.Status')
    dc_yml=$(echo "$line" | jq -r '.EntryPoint')

    # Si le stack n'est toujours pas démarré, on modifie les TAG des images
    if [ ! "$status_stack" = "1" ]; then
        # On modifie les tag dans le DC
        if (cat "$DOSSIER_DC"/"$num_stack"/"$dc_yml" | grep "image" | grep ":4.0"); then
            sed -i "s|:4.0|:4.3|g" "$DOSSIER_DC"/"$num_stack"/"$dc_yml"
        elif (cat "$DOSSIER_DC"/"$num_stack"/"$dc_yml" | grep "image" | grep ":4.3"); then
            sed -i "s|:4.3|:4.0|g" "$DOSSIER_DC"/"$num_stack"/"$dc_yml"
        elif (cat "$DOSSIER_DC"/"$num_stack"/"$dc_yml" | grep "image" | grep ":dev"); then
            sed -i "s|:dev|:4.0|g" "$DOSSIER_DC"/"$num_stack"/"$dc_yml"
        fi
        # On retente un démarrage
        echo "Démarrage du stack $nom_stack..." >>"$LOG"
        START=$(
            curl --noproxy "*" -s -k \
                "$P_URL/api/stacks/$num_stack/start?endpointId=1" \
                -X POST \
                -H "Authorization: Bearer $T"
        )
        echo -e "\nRetour du démarrage : $START" >>"$LOG"
    else
        echo -e "Le stack $nom_stack est déjà démarré." >>"$LOG"
    fi
done

echo -e "\nLe traitement est terminé. Les sites peuvent être démarrés à partir de l'interface qui doit être actualisée."

# Nettoyage (on garde juste les logs)
rm -rf $DOSSIER_TEMP
