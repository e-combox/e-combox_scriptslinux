#!/bin/bash

# Ce script rétablit les droits d'Odoo

# Récupère la liste des noms de volumes qui commencent par "odoo" et ne se terminent pas par "db"
   # shellcheck disable=SC2010
   volumes=$(ls -1 /var/lib/docker/volumes/ | grep -E '^odoo.*[^db]$')

   # Met les bons droits sur la liste des volumes récupérés
   echo -e "Rétablissement des droits sur les volumes Odoo... En cours"
   for volume in $volumes; do
      chown -R 101 "/var/lib/docker/volumes/$volume"
      chgrp -R 101 "/var/lib/docker/volumes/$volume"
   done
   echo -e "Rétablissement des droits sur les volumes Odoo... Fait"