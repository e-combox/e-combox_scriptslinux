#!/bin/bash

# # Ce script réintroduit les addons d'Odoo à la racine

# Récupère la liste des noms de volumes qui commencent par "odoo" et se terminent par "addons"
# shellcheck disable=SC2010
volumes=$(ls -1 /var/lib/docker/volumes/ | grep -E '^odoo.*[addons]$')

# Vérifie que les addons ne sont pas déjà à la racine
# Et les réintègre à la racine.
# Met ensuite les bons droits sur la liste des volumes récupérés
echo -e "Réintégration des addons à la racine si nécessaire"
for volume in $volumes; do
   chemin_volume="/var/lib/docker/volumes/$volume"

   if [ -e "$chemin_volume/_data/addonsPourV15" ]; then
      mv "$chemin_volume"/_data/addonsPourV15/* "$chemin_volume/_data/"
      rmdir "$chemin_volume"/_data/addonsPourV15
      chown -R 101 "$chemin_volume"
      chgrp -R 101 "$chemin_volume"
      echo -e "Réintégration des addons à la racine pour $volume... Fait"
      reintegration=true
   fi
   if [ -e "$chemin_volume/_data/addonsPourV16" ]; then
      mv "$chemin_volume"/_data/addonsPourV16/* "$chemin_volume/_data/"
      rmdir "$chemin_volume"/_data/addonsPourV16
      chown -R 101 "$chemin_volume"
      chgrp -R 101 "$chemin_volume"
      echo -e "Réintégration des addons à la racine pour $volume... Fait"
      reintegration=true
   fi
done
if [ "$reintegration" = true ]; then
   echo -e "Réintégration des addons à la racine... Terminé"
else
   echo -e "Réintégration des addons à la racine... Non nécessaire"
fi
