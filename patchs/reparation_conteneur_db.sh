#!/bin/bash

# Permet au site d'ếtre dupliqué même si système de base de données est MySQL au lieu de MariaDB
# Attention, le script doit être exécuté de nouveau si le site est redémarré
# Appel du script : bash reparation_conteneur_db.sh nom_conteneur_db 

CONTENEURBD=$1

docker exec "$CONTENEURBD" rm /etc/apt/sources.list.d/mariadb.list
docker exec "$CONTENEURBD" apt update
docker exec "$CONTENEURBD" apt install curl -y
docker exec "$CONTENEURBD" bash -c  "curl -LsS https://r.mariadb.com/downloads/mariadb_repo_setup | bash -s -- --mariadb-server-version=10.4"
docker exec "$CONTENEURBD" apt update
docker exec "$CONTENEURBD" apt install mariadb-server -y