#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-combox"
# Le nom du dossier d'installation a été modifié
if [ -d "/opt/e-comBox" ]; then
    mv /opt/e-comBox $DOSSIER_INSTALL
fi
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"

SHOW_USAGE() {
    echo -e "\nCe script permet la sauvegarde de l'intégralité des sites."
    echo -e "Si le chemin complet (y compris le nom de l'archive en \".tar.gz\.) est fourni, le script se déroulera sans interactivité. (voir Usage ci-après)."
    echo -e "Usage: bash $0 [-a \"nom_archive.tar.gz\"] [-h]."
    echo -e "\t-a\t\tChemin vers l'archive de sauvegarde [-a \"nom_archive.tar.gz\"]."
    echo -e "\t-h\t\tDétail des options"
}

# Initialisation des paramètres passés au script
while getopts "a:h" option; do
    # a: Chemin vers l'archive de sauvegarde
    # h: help

    case $option in
    a)
        archive_o="$OPTARG"
        ;;
    h)
        SHOW_USAGE
        exit 1
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        SHOW_USAGE
        exit 1
        ;;
    esac
done

if [ -z "$archive_o" ]; then
    archive_o="false"
else
    # Test du nom de l'archive
    if [[ ! "${archive_o%.tar.gz}" != "$archive_o" ]]; then
        echo "${archive_o%.tar.gz}"
        echo -e "${COLSTOP}\nLe nom d'archive n'est pas valide. Il doit se terminer par '.tar.gz'. Veuillez réessayer.\n${COLCMD}"
        exit 1
    fi
    # Test du chemin
    DOSSIER_ARCHIVE=$(dirname "$ARCHIVE")
    if [ ! -d "$DOSSIER_ARCHIVE" ]; then
        echo -e "${COLSTOP}\nDossier $archive_o... Non trouvé${COLCMD}"
        exit 1
    fi
fi

SAUV_SITES "$archive_o"