# V5

## 5.0.0

**Date de sortie prévue** : septembre 2024

### Fonctionnalités

- **Interface dédiée aux élèves** : les élèves pourront retrouver les sites déployés pour eux par les professeurs et agir dessus.
- **Compte prof** : possibilité de créer des comptes élèves.
- **Groupement des sites** : possibilité pour l'enseignant de créer des groupes afin de classer plus facilement les sites.
- **Accès aux sites** : possibilité pour l'enseignant de choisir si un site doit être publié sur Internet ou pas.
- **Modification du nommage des sites** : possibilité de choisir les noms pour les sites lorsque l'on en crée plusieurs à partir d'une même configuration.
- **Création des sites** : levée de la limite de 30 sites max en simultanée lors de la création.
- **Interface graphique de gestion** : permettre certaines opérations (comme la sauvegarde/restauration et la mise à jour) à partir de l'interface.
- **Redémarrage automatique des sites** : redémarrage automatique des sites sauf si'l ont été volontairement stoppés par l'utilisateur.

### Nouvelles applications (en mode expérimental)

- **Nouvelle application de comptabilité** : à priori OpenConcerto.
- **IA Mistral**

### Sécurité

- **ajout d'un logiciel de prévention des intrusions**
- **Lancement de Docker avec un utilisateur non-privilégié**
- **Mise à jour des images** : réduction des vulnérabilités.

### Divers

- **Utilisation d'une API intermédiaire personnelle**
- **Mise à jour des images** : mise à jour des images Prestashop (8.0), WordPress (6.4.3), Kanboard (1.2.35), Mautic (5.0.3).
- **Création automatique de sites** : Les sites pourront être créés automatiquement en ligne de commande via un fichier csv passé en paramètre.

# V4

C'est la version qui permet de déployer l'e-comBox à grande échelle dans les académies et dans les régions.

## [4.3.0]

**Date de sortie** : mars 2023

### Fonctionnalités

- **Réinitialisation d'un site** : ajout d'un bouton sur la carte d'un site permettant de le restaurer à son état initial.
- **Accès aux sites** : ajout d'un lien supplémentaire pour l'accès direct des sites au front-end.
- **Ajout du lien vers le site recensant les modèles**
- **Alimentation automatique des mentions légales à partir de param.conf** : utile pour ceux qui déploient de nombreuses instances avec des mentions légales identiques.

### Sécurité

- **Mise à jour des images** : réduction des vulnérabilités.

### Divers

- **Reverse-Proxy e-comBox** : configuration de nginx permettant d'accéder à différents services tiers à partir de l'application e-comBox (suppression de l'utilisation de cors-anywhere)
- **Formatage des Docker Compose** : passage des fichiers Docker Compose au dernier format compatible (version v2 de Docker compose).  
- **Optimisations diverses**

## [4.2.2]

**Date de sortie** : septembre 2023

### **Divers**

- **Résolution des bugs**
- **Optimisations diverses**

## [4.2.1]

**Date de sortie** : Juillet 2023

### **Divers**

- **Résolution des bugs**
- **Optimisations diverses**
- **Améliorations liées à la sécurité des images Docker**

## [4.2.0]

**Date de sortie** : juillet 2023

### **Fonctionnalités**

- **Authentification** : module permettant l'authentification des professeurs sur l'interface e-comBox via OAuth.
- **Automatisation de la sauvegarde/restauration**
- **Possibilité de ne restaurer qu'un site**

### **Divers**

- **Portainer** : mise à jour de la version, intégration de traces plus développées, intégration des certificats Let's Encrypt intermédiaires, possibilité de lui ajouter facilement un certificat, ne démarre plus via un docker-compose.
- **Optimisations diverses**
- **Améliorations liées à la sécurité des images Docker**

## [4.1.0]

**Date de sortie** : février 2023

### **Fonctionnalités**

- **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub.
- **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.
- **Actions sur les sites** : possibilité de réaliser une action (supprimer, stopper, démarrer) sur plusieurs sites en les sélectionnant.
- **Accès aux sites** : accès aux identifiants de connexion des différents sites à partir de l'interface de l'e-comBox.

### **Divers**

- **Mise à jour des applications**
- **Mise à niveau vers Angular 13**
- **Installation facilitée** : installation de l'e-comBox sans intervention de l'administrateur.
- **Déploiement automatisé** : exemple de script et de documentation pour un déploiement via Ansible.
- **Certificats** : gestion optimisée et facilitée avec un script permettant de générer un certificat avec letsEncrypt.
- **Performances** : amélioration de la fluidité d'utilisation de l'interface e-comBox.

## [4.0.0]

**Date de sortie** : 20 novembre 2022

### **Fonctionnalités**

- **Reverse Proxy** : possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports (mis à part le port d'accès à l'interface) sur le serveur et à ne dédier qu'une seule adresse IP publique pour l'ensemble des instances.
- **Authentification** : module permettant l'authentification des professeurs sur l'interface via un système d'authentification multi-utilisateurs interne à l'e-comBox.
- **SFTP** : accès au SFTP via le Web (plus besoin de client sftp et de ports supplémentaires ouverts)
- **Portainer** : synchronisation automatique de l'application avec le mot de passe défini par l'administrateur dans Portainer.
- **Sites** : visualisation des identifiants de connexion de chaque type de site directement à partir de l'application e-comBox.

### **Sécurisation**

- **HTTPS** : Intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox)

# V3

## [Pour toutes les versions]

- **Kanboard** : mise à jour vers la version 1.2.21, intégration des plugins et possibilité de les installer via l'interface.

## [3.2.0]

### **Fonctionnalités**

- **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub
- **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.

## [3.1.0]

- **Date de sortie**: 28/02/2022

**Lien vers les "issues"** : <https://forge.aeif.fr/groups/e-combox/-/issues>

### **Sécurisation**

- Intégration du protocole HTTPS au niveau des sites (**V3.1**).

### **Bug**

- Fix version in the footer

### **Sites**

- **Tous les types de site** : mise à jour des versions.

- **Général** : sauvegarde/restauration d'un seul site (en demandant un chemin à l’utilisateur).
- **Prestashop** : intégration d'un module tchat sur Prestashop.

### **Fonctionnalités**

- **Gérer vos modèles de site** : possibilité de renommer les images personnalisées par les profs (**V3.1**).
- **Sauvegarde et restauration d'un seul site.
- **En bas du menu de gauche** (sous Aide) : Intégration de CGU personnalisables (**V3.1**).
- **Mise à jour sur Linux** : automatiser les mises à jour (aucune action nécessaire pour l'administrateur sauf à lancer la mise à jour)

### **Divers**

- Rendre l'application un peu plus "responsive".
- Automatiser l'ajout des images personnalisées.
- Automatiser le partage d'un modèle de site créé avec l'e-comBox.
