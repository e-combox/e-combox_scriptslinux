#!/bin/bash

# Définition des paramètres utiles à l'application.

# Version du fichier paramètre (ne pas changer la valeur).
VERSION_PARAM="4.3.0"

# Validation (ou non) de la licence.
# Si cette variable n'a pas la valeur "true", l'application ne pourra pas être installée.
# La licence est consultable ici : https://forge.apps.education.fr/e-combox/e-combox_scriptslinux/-/raw/4.3/LICENCE.
# Les conditions plus générales de la licence CeCILL sont définies sur le site : http://www.cecill.info.
VALIDATION_LICENCE="true"

# Mot de passe pour la connexion en admin à e-comBox et Portainer.
# Le mot de passe doit contenir au moins de 12 caractères sans espace.
# Le mot de passe ne doit pas contenir les caractères spéciaux suivants : \ " ` $
# Il ne peut pas être égal à "portnairAdmin" qui est connu de tous
# Le système met le mot de passe à jour avec celui saisi si les deux conditions suivantes sont réunies
# 1 - À l'installation ou à la réinitialisation de l'application
# 2 - Si le mot de passe renseigné a été au préalable modifié sur Portainer
# Ce mot de passe en clair sera chiffré, puis supprimé à l'installation, à la réinitialisation ou à la reconfiguration de l'application
# C'est donc normal qu'une fois l'application installée, réinitialisée ou mise à jour, ce paramètre apparaisse vide.
MDP_PORTAINER=""

# Adresse IP privée ou nom de domaine correspondant à une adresse IP privée.
# Si vous saisissez un nom de domaine, celui-ci doit pouvoir être résolu.
# Si aucun domaine n'est configuré, les sites seront accessibles à partir du réseau local via cette adresse IP privée.
# L'adresse IP privée doit obligatoirement être configurée même si le paramètre suivant est renseigné.
ADRESSE_IP_PRIVEE=""

# Adresse IP publique ou nom de domaine correspondant à une adresse IP publique.
# Si vous saisissez un nom de domaine, celui-ci doit pouvoir être résolu.
# Il s'agit du nom de domaine ou de l'adresse IP qui sera utilisée pour la connexion à l'interface, à Portainer et aux sites à la place de l'adresse IP privée.
# Aucune valeur ne doit donc être saisie ici si vous ne voulez pas cela, notamment si le serveur ne doit pas être accessible de l'extérieur.
DOMAINE=""

# Utilisation d'un Reverse-Proxy externe configuré par vos soins - O/N (N par défaut).
# Si vous passez par un reverse proxy externe, mettre "O".
# Les minuscules sont également acceptées.
RP_EXT="N"

# Chemin d'accès éventuel (en cas d'utilisation d'un Reverse-Proxy externe).
# Ne pas mettre de "/" dans le chemin.
CHEMIN=""

# Dossier en chemin absolu contenant la clé secrète qui chiffre le mot de passe de Portainer.
# Par défaut /opt/e-combox, il est conseillé de le modifier.
# En cas de modification après installation, ne pas oublier de déplacer le fichier contenant la clé secrète.
DOSSIER_MDP_KEY="/opt/e-combox"

# Dossier en chemin absolu contenant le mot de passe chiffré.
# Par défaut /opt/e-combox, il est conseillé de le modifier.
# En cas de modification après installation, ne pas oublier de déplacer le fichier contenant le mot de passe chiffré.
DOSSIER_MDP_CHIFFRE="/opt/e-combox"

# Port utilisé pour un accès direct à Portainer (8880 par défaut).
# Ce port n'a vocation à n'être utilisé qu'en cas de dépannage si l'accès via le reverse-proxy est impossible.
PORT_PORTAINER="8880"

# Port utilisé pour l'accès à Portainer, à l'interface et aux sites (8800 par défaut).
# Il s'agit du seul port exposé.
# Si aucun service Web n'écoute sur le port 443 de votre serveur, ce dernier peut être utilisé.
PORT_RP="8800"

# Port utilisé pour le registry (5443 par défaut).
PORT_REGISTRY="5443"

# Adresse du Proxy.
# Saisissez ip-proxy:port.
ADRESSE_PROXY=""

# Hôtes à ignorer par le proxy.
# Ce paramètre n'est à renseigner que si une adresse de Proxy a été saisie.
# Saisissez le ou les hôtes séparés par une virgule sans espace.
NO_PROXY=""

# Adresse du réseau interne à Docker.
# Cette adresse réseau doit être éventuellement modifiée notamment pour adapter le masque.
# Un site nécessite 2 adresses IP réseau. 
# Compte tenu des conteneurs utilitaires, avec un masque en /24, le nombre de sites est limité à 120.
# Cette adresse peut être modifiée ultérieurement.
NET_ECB="192.168.97.0/24"

# Suppression des images non associées à un site en cours d'exécution (false par défaut) 
# Cela permet de gagner de l'espace mais aussi de supprimer des images qui ne seront plus jamais utilisées
# Le premier démarrage des sites sera un peu plus long car les images correspondantes devront être de nouveau téléchargées.
# Définir le paramètre à "true" si vous voulez supprimer ces images (conseillé lors d'une migration).
DEL_IMAGES="false"

# Les 2 variables qui suivent sont utiles pour donner le chemin vers les éléments pour mettre en place un certificat existant.
# Vous pouvez les laisser vides dans trois cas.
# Cas 1 : si vous passez par un reverse proxy externe, les certificats configurés dans ce dernier sont propagés.
# Cas 2 : l'accès à l'e-comBox ne va se faire que via le réseau local.
# Cas 3  vous ne disposez pas de certificat pour votre nom de domaine.
# À noter que le script manage_certificat.sh crée un certificat Let's Encrypt et permet de mettre à jour l'application
# si une des 6 variables qui suivent ont été modifiées
# Un certificat auto-signé se créera alors automatiquement via les 4 variables qui suivent.

# Fichier certificat existant avec le chemin complet /chemin/fichier.crt ou /chemin/fichier.pem.
CHEMIN_CERT=""

# Fichier de la clé privée existante correspondante au certificat avec le chemin complet /chemin/fichier.key.
CHEMIN_KEY=""

# Uniquement si utilisation de manage_certificat.sh pour créer un certificat Let's Encrypt
# Adresse de courriel non obligatoire pour créer le certificat (mais recommandé par Lets'encrypt)
MAIL=""

# Les 4 variables suivantes sont utiles pour créer un certificat auto signé.
# Elles sont utilisés pour le certificat du registry local.
# Elles sont également utilisées pour le reverse proxy (Nginx) si CHEMIN_CERT et CHEMIN_KEY sont vides.
# Même si vous avez un certificat existant, elles doivent obligatoirement être renseignées.
# Vous pouvez les modifier comme il vous convient.

# Code Pays sur 2 lettres.
CODE_PAYS="FR"

# Nom Pays.
NOM_PAYS="France"

# Nom région.
NOM_REGION="Corse"

# Nom organisation.
NOM_ORGANISATION="ReseauCerta"

# Ajout du mode d'authentification pour les comptes non-admin basés sur le protocole OAuth (false par défaut).
# Si false, il n'est pas tenu compte des paramètres qui suivent.
# Si true, vous devez fournir la valeur des paramètres qui suivent.
# Veuillez-vous référer à votre fournisseur OAuth pour déterminer les bonnes valeurs.
OAUTH_ENABLE="false"

# true ou false
# Lors de l'utilisation de SSO (variable à "true"), le fournisseur OAuth n'est pas obligé de demander des informations d'identification.
SSO_ENABLE=""

# Identifiant public de l'application OAuth.
CLIENT_ID=""

# Client Secret
CLIENT_SECRET=""

# URL utilisée pour s'authentifier auprès du fournisseur OAuth.
# L'utilisateur sera redirigé vers la page d'authentification du fournisseur d'identité.
AUTHORIZATION_URL=""

# URL utilisée par Portainer pour échanger un code d'authentification OAuth valide contre un token d'accès.
ACCESS_TOKEN_URL=""

# URL utilisée par Portainer pour récupérer des informations sur l'utilisateur authentifié.
RESOURCE_URL=""

# URL utilisée par Portainer pour rediriger l'utilisateur vers le fournisseur OAuth afin de déconnecter l'utilisateur de la session du fournisseur d'identité.
LOGOUT_URL=""

# Identifiant qui sera utilisé par Portainer pour créer un compte pour l'utilisateur authentifié. 
# Extrait du serveur de ressources spécifié via le champ URL de la ressource.
USER_IDENTIFIER=""

# Étendues requises par le fournisseur OAuth pour récupérer des informations sur l'utilisateur authentifié.
SCOPES=""

# Fichier certificat du fournisseur 0Auth avec le chemin complet /chemin/fichier.pem.
# Ce dernier doit potentiellement être intégré à Portainer
CERT_OAUTH=""

# Les variables qui suivent servent à alimenter automatiquement les mentions légales
# si ces dernières n'existent pas ou si le script modif_mentions_legales.sh est exécuté.
# Les mentions légales peuvent également être modifiées via l'interface de l'e-comBox.
PROPRIETAIRE=""
ADRESSE_POSTALE=""
IMMATRICULATION=""
HEBERGEUR=""
RESPONSABLE_TRAITEMENT=""
ADMIN_SERVICE=""
DPO=""